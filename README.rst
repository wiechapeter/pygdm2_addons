***********************************
About
***********************************

Additional tools / scripts for pyGDM2.

pyGDM2 is available on `pypi <https://pypi.python.org/pypi/pygdm2/>`_ and `gitlab <https://gitlab.com/wiechapeter/pyGDM2>`_. 
See also the `pyGDM2 documentation website <https://wiechapeter.gitlab.io/pyGDM2-doc/>`_.



Requirements
================================

- python 3.5+

Python modules
------------------

- **pyGDM2** (1.1+)
- **numpy**


Author
=========================

   - P\. R. Wiecha
   


