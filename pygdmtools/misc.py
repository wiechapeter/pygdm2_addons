# encoding: utf-8
"""
other tools in relation with pygdm

P. Wiecha, 2021

"""
import numpy as np
from scipy.spatial.distance import pdist, cdist


def quasirandom_positions(N, average_dist, min_dist, min_dist_y=None, max_try=2000, 
                          move_to_center=True, rounding_decimals=1, 
                          verbose=False, mode='radius'):
    
    
    # =============================================================================
    # generate quasirandom arrangement
    # =============================================================================
    if mode=='radius':
        ## sidelength of pillar area to yield target average distance
        L = np.sqrt(N) * average_dist * np.sqrt(2)
        pos = [[0,0], [0,0]]
        i_retry = 0
        while pdist(pos).min() <= min_dist and i_retry < max_try:
            i_retry += 1
            _pos = []
            for i in range(N):
                x, y = np.random.random(2) * L
                _pos.append([x, y])
            pos = np.array(_pos)
    
    if mode=='grid':
        def generate_points_with_min_distance(n, shape, min_dist, min_dist_y):
            ## from https://stackoverflow.com/questions/27499139/how-can-i-set-a-minimum-distance-constraint-for-generating-points-with-numpy-ran
            # compute grid shape based on number of points
            width_ratio = shape[1] / shape[0]
            num_y = np.int32(np.sqrt(n / width_ratio)) + 1
            num_x = np.int32(n / num_y) + 1
        
            # create regularly spaced neurons
            x = np.linspace(0., shape[1]-1, num_x, dtype=np.float32)
            y = np.linspace(0., shape[0]-1, num_y, dtype=np.float32)
            coords = np.stack(np.meshgrid(x, y), -1).reshape(-1,2)
        
            # compute spacing
            init_dist = np.min((x[1]-x[0], y[1]-y[0]))
        
            # perturb points
            if min_dist_y is None:
                min_dist_y = min_dist
            max_movementX = (init_dist - min_dist)/2
            max_movementY = (init_dist - min_dist_y)/2
            noise_x = np.random.uniform(low=-max_movementX, high=max_movementX, size=(len(coords)))
            noise_y = np.random.uniform(low=-max_movementY, high=max_movementY, size=(len(coords)))
            coords.T[0] += noise_x
            coords.T[1] += noise_y

            return coords
        
        ## use given lengthscale as square sidelength
        L = average_dist
        pos = generate_points_with_min_distance(n=N, shape=(L, L), min_dist=min_dist, min_dist_y=min_dist_y)
        
    
    ## center between min/max
    if move_to_center:
        pos.T[0] -= pos.T[0].max()
        pos.T[1] -= pos.T[1].max()
        pos.T[0] += (pos.T[0].max() - pos.T[0].min()) / 2.
        pos.T[1] += (pos.T[1].max() - pos.T[1].min()) / 2.
    pos = np.around(pos, rounding_decimals)
    
    # =============================================================================
    # verbose: print results
    # =============================================================================
    if pdist(pos).min() < min_dist - (N*10**(-rounding_decimals)):
        if verbose:
            print ("failed to distribute structures in {} iterations! try to increase min. distance.".format(i_retry))
        raise ValueError("random distribution error")
    else:
        if verbose:
            print ("success after {} iterations.".format(i_retry))
            avg_nn = [np.sort(cdist([p], pos))[0][1] for p in pos]
            print ("              average / min / max distance:")
            print ("next neighbor: {:.1f} / {:.1f} / {:.1f} (nm)".format(
                    np.mean(avg_nn), np.min(avg_nn), np.max(avg_nn)))
            print ("full ensemble: {:.1f} / {:.1f} / {:.1f} (nm)".format(
                    pdist(pos).mean(), pdist(pos).min(), pdist(pos).max()))
        
    return pos




# =============================================================================
# coordinate-list to 3D space representation
# =============================================================================
def list_to_threeD(sim, field_index, which_field='e', 
                   wl=None, verbose=False,
                   minX=None, minY=None, minZ=None,
                   maxX=-1, maxY=-1, maxZ=-1):
    """convert coordinate-lists (geometry and fields) to 3D representations
    
    Note: Geometry x and y coordinates must be on a cubic grid of even multiples of "step"
    
    which_field: 'e' (default) or 'h'
    
    minX, minY, minZ : if given, uses the given value as 'starting' point for the geometry volume
    maxX, maxY, maxZ : if given, determines the minimum size of the volume (given in nm)
    
    """
    if verbose:
        print("conversion to volumetric arrays...", end='')
    if field_index is not None:
        wavelength = sim.E[field_index][0]['wavelength']
    else:
        wavelength = wl
    geopos       = sim.struct.geometry.copy()
    step         = sim.struct.step
    
    ## determine size of volume containing the structure
    if minX is None:
        minX = geopos.T[0].min()
    if minY is None:
        minY = geopos.T[1].min()
    if minZ is None:
        minZ = geopos.T[2].min()
    maxX = max([int(maxX//step), int(np.round(geopos.T[0].max() / step)) + 1])
    maxY = max([int(maxY//step), int(np.round(geopos.T[1].max() / step)) + 1])
    maxZ = max([int(maxZ//step), int(np.round(geopos.T[2].max() / step - 0.25)) + 1])
    
    ## offset to starting point
    geopos.T[0] -= minX
    geopos.T[1] -= minY
    geopos.T[2] -= minZ
    
    
    ## --- 3D structure geometry
    voxels = np.zeros(shape=(maxX, maxY, maxZ), dtype=np.uint8)  # only geometry (0=none, 1=material)
    voxels_eps = np.zeros(shape=(maxX, maxY, maxZ), dtype=np.complex64)  # permittivity at each pos
    
    ## --- 3D internal complex electric field
    voxels_field = np.zeros(shape=(maxX, maxY, maxZ, 3), dtype=np.complex64)
    
    if field_index is not None:
        if which_field.lower() == 'e':
            field = sim.E
        else:
            field = sim.H
    
    for i_pos, pos in enumerate(geopos):
        eps = sim.struct.material[i_pos].epsilon(wavelength)
        pos = np.round(pos / sim.struct.step)
        pos = pos.astype(int)
        voxels[pos[0],pos[1],pos[2]] = 1
        voxels_eps[pos[0],pos[1],pos[2]] = eps
        
        if field_index is not None:
            voxels_field[pos[0],pos[1],pos[2], 0] = field[field_index][1][i_pos][0]
            voxels_field[pos[0],pos[1],pos[2], 1] = field[field_index][1][i_pos][1]
            voxels_field[pos[0],pos[1],pos[2], 2] = field[field_index][1][i_pos][2]
        
    return voxels, voxels_eps, voxels_field


def list_to_threeD_geometry_only(geo, verbose=False,
                                 minX=None, minY=None, minZ=None,
                                 maxX=-1, maxY=-1, maxZ=-1):
    """convert coordinate-list of geometry (or sim) to 3D voxel representation
    
    Note: Geometry x and y coordinates must be on a cubic grid of even multiples of "step"
    
    minX, minY, minZ : if given, uses the given value as 'starting' point for the geometry volume
    maxX, maxY, maxZ : if given, determines the minimum size of the volume (given in nm)
    
    """
    if verbose:
        print("conversion to volumetric arrays...", end='')
    from pyGDM2 import tools
    geopos = tools.get_geometry(geo, return_type='tuples')
    step   = tools.get_step_from_geometry(geo)
    
    ## determine size of volume containing the structure
    if minX is None:
        minX = geopos.T[0].min()
    if minY is None:
        minY = geopos.T[1].min()
    if minZ is None:
        minZ = geopos.T[2].min()
    maxX = max([int(maxX//step), int(np.round(geopos.T[0].max() / step)) + 1])
    maxY = max([int(maxY//step), int(np.round(geopos.T[1].max() / step)) + 1])
    maxZ = max([int(maxZ//step), int(np.round(geopos.T[2].max() / step)) + 1])
    
    ## offset to starting point
    geopos.T[0] -= minX
    geopos.T[1] -= minY
    geopos.T[2] -= minZ
    
    
    ## --- 3D structure geometry
    voxels = np.zeros(shape=(maxX, maxY, maxZ), dtype=np.uint8)  # only geometry (0=none, 1=material)
    
    for i_pos, pos in enumerate(geopos):
        pos = np.round(pos / step)
        pos = pos.astype(int)
        voxels[pos[0], pos[1], pos[2]] = 1
        
    return voxels









# =============================================================================
# 3D space to coordinate-list representation
# =============================================================================
def get_3d_field_3dmaterials(Xval, Y_out, step=10, H0=0.5,
                             order='alter re im'):
    
    idx_mat = np.logical_or(Xval[:,:,:,0]!=0, Xval[:,:,:,1]!=0)
    
    NX = Xval.shape[0]
    NY = Xval.shape[1]
    NZ = Xval.shape[2]
    xl = np.linspace(step, step*(NX), NX)
    yl = np.linspace(step, step*(NY), NY)
    zl = np.linspace(0, step*(NZ-1), NZ)
    xP, yP, zP = np.meshgrid(xl, yl, zl)
    
    if order=='alter re im':
        Ex = Y_out[:,:,:,0] + 1j*Y_out[:,:,:,1]
        Ey = Y_out[:,:,:,2] + 1j*Y_out[:,:,:,3]
        Ez = Y_out[:,:,:,4] + 1j*Y_out[:,:,:,5]
    if order=='all re all im':
        Ex = Y_out[:,:,:,0] + 1j*Y_out[:,:,:,3]
        Ey = Y_out[:,:,:,1] + 1j*Y_out[:,:,:,4]
        Ez = Y_out[:,:,:,2] + 1j*Y_out[:,:,:,5]
    else:
        raise Exception("unknown order of electric field components!")
    
    ## --- reconstruct list of field-vectors
    Es = np.transpose([yP.flatten(), xP.flatten(), zP.flatten(), 
                       Ex.flatten(), Ey.flatten(), Ez.flatten()])
    
    ## --- reconstruct coordinates of structure
    geomat_cmplx = np.sqrt(Xval[:,:,:,0] + 1j * Xval[:,:,:,1])
    
    material_list = geomat_cmplx.flatten()[idx_mat.flatten()]
    geo = np.real(Es.T[:3].T[idx_mat.flatten()])
    geo.T[2] += step*H0
    Es_int = Es.T[3:].T[idx_mat.flatten()].astype(np.complex64)
    
    
    return geo, material_list, Es_int










# =============================================================================
# legacy support
# =============================================================================
def _convert_pyGDM_1to2(sim, ef=None, Ein=None, EinKwargs={}):
    """Convert pyGDM1 sim/efield data to a pyGDM2 simulation object
    
    Parameters
    ----------
    sim : dict
        pyGDM1 simulation  dictionary
    
    ef : list, default: None
        optional: pyGDM1 simulation results
    
    Ein : pyGDM2 field_generator, default: None
        pyGDM1 or pyGDM2 field-generator. 
        If None, use plane wave :func:`.fields.planewave`.
        
    EinKwargs : list, default: None
        optional: kwargs for the pyGDM1 or pyGDM2 field_generator `Ein`.
        in case the pyGDM2 interface is used, **DO NOT** define the kwargs 
        "wavelength" or "theta".
        
    Returns
    -------
    instance of :class:`.core.simulation`
    """
#    from pyGDM import main

    ## ---------- Setup structure
    from pyGDM2 import materials
    from pyGDM2 import structures
    from pyGDM import fields as fields1
    from pyGDM2 import fields
    from pyGDM2 import core
    
    if sim['norm'] == 1:
        mesh = 'cube'
    else:
        mesh = 'hex'
    norm = structures.get_normalization(mesh)
    
    geometry = np.transpose([sim['xm'], sim['ym'], sim['zm']])
    step = sim['step']
    spacing = sim['spacing']
    n1, n2, n3 = sim['n1'], sim['n2'], sim['n3']
    
    if sim['material'] == 'au':
        material = materials.gold()
    elif sim['material'] == 'si':
        material = materials.silicon()
    elif sim['material'] == 'al':
        material = materials.alu()
    elif sim['material'] in ['TT', '15']:
        material = materials.dummy(1.5)
    elif sim['material'] == '20':
        material = materials.dummy(2.0)
    elif sim['material'] == '30':
        material = materials.dummy(3.0)
    elif sim['material'] == '35':
        material = materials.dummy(3.5)
    else:
        raise ValueError("unknown pyGDM1 material string: '{}'".format(sim['material']))
        
    
    struct = structures.struct(step=step, geometry=geometry, material=material, 
                               n1=n1, n2=n2, n3=n3, normalization=norm, 
                               spacing=spacing)
    
    
    ## ---------- Setup incident field
    if EinKwargs.has_key('wavelength') or EinKwargs.has_key('theta'):
        warnings.warn("Forbidden entry: Incident field configuration dictionary " + 
                      "MUST NOT contain 'wavelength' or 'theta' key, since this " 
                      + "is taken from pyGDM1 simulation. Ignoring these values.")
        if EinKwargs.has_key('wavelength'):
            del EinKwargs['wavelength']
        if EinKwargs.has_key('theta'):
            del EinKwargs['theta']
    kwargs = EinKwargs
    
    if Ein is None or EinKwargs=={} or Ein == fields1.getE0planewave:
        warnings.warn("No field-configuration obtained. Falling back to default plane wave illumination.")
        field_generator = fields.planewave
        kwargs['theta'] = sim['atheta']
    elif Ein == fields1.getE0focused:
        field_generator = fields.focused_planewave
        kwargs['theta'] = sim['atheta']
    elif Ein == fields1.getE0gaussian:
        field_generator = fields.gaussian
        kwargs['theta'] = sim['atheta']
    elif Ein == fields1.getE0elecDipole:
        field_generator = fields.dipole_electric
    elif Ein == fields1.getE0magDipole:
        field_generator = fields.dipole_magnetic
    else:
        field_generator = Ein
    
    
    wavelengths = sim['elambda']
    efield = fields.efield(field_generator, wavelengths=wavelengths, kwargs=kwargs)
    
    
    ## ---------- Simulation initialization
    sim = core.simulation(struct, efield)
    
    if ef is not None:
        warnings.warn("Received Efield-data. Transferring scattered fields from " + 
                      "pyGDM1 to pyGDM2 not yet supported. Rerunning scattering " +
                      "simulation in pyGDM2 to obtain fields. This may take a while.")
        core.scatter(sim)
    
    return sim























#%%
if __name__ == "__main__":
    import matplotlib.pyplot as plt
    
    for i in range(4):
        for j in range(4):
            plt.subplot(4,4,i*4+j+1, aspect='equal')
            pos = quasirandom_positions(N=7, average_dist=500, min_dist=420, verbose=0)
            plt.scatter([pos.T[0]], [pos.T[1]])
            plt.xlim(-1500, 1500); plt.ylim(-1500, 1500)
            plt.xticks([]); plt.yticks([])
    plt.tight_layout()
    plt.show()
    
        
    
    