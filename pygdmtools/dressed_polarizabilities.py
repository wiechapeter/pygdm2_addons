# encoding: utf-8
#
#Copyright (C) 2019-2021, P. R. Wiecha
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
polarizability extraction and re-coupling tools for pyGDM2
"""
from __future__ import print_function, division

import numpy as np
import copy
import time
import warnings

from pyGDM2 import structures
from pygdmtools.polarizabilities import get_center_of_mass_list, get_center_of_energydensity





def get_dipole_moments_subgeo(sim, sub_geo_list=None, 
                                method='lu', matrix_setup='numba', 
                                return_full_sim=False,
                                return_dressed_simulations=False,
                                verbose=0):
    from pyGDM2 import linear_py
    from pyGDM2 import tools
    
    
    if sub_geo_list is not None:
        sub_geo_list = np.array(sub_geo_list)
        if len(sub_geo_list.shape) == 2:
            sub_geo_list = sub_geo_list[None,:,:]   # add an additional first dimension
        if sub_geo_list.shape[2] != 3:
            raise Exception("geo split list must be composed of (x,y,z) coordinate tuples!")
    else: 
        sub_geo_list = sim.struct.geometry.copy()[None,:,:]
    
    wavelengths = sim.efield.wavelengths
    k0_spectrum = 2 * np.pi / np.array(wavelengths).copy()
    
    ## --- if not done, run the FULL scattering simulation ---
    if sim.E is None:
        if verbose:
            print(" --- efields not calculated. run full simulation ---")
        sim.scatter(method=method, matrix_setup=matrix_setup, verbose=verbose)

    dict_P_M_list = []
    dressed_simulations = []
    for i_subgeo, geo_split in enumerate(sub_geo_list):
        if verbose:
            print("   sub-geometry {}/{}".format(i_subgeo+1, len(sub_geo_list)))
        ## use fraction of geometry for polarizability calculation:
        sim_p, _sim_rest = tools.split_simulation(sim, geo_split)
        if return_dressed_simulations:
            dressed_simulations.append(copy.deepcopy(sim_p))
        
        r0 = np.average(sim_p.struct.geometry, axis=0)
        
        
        P = []
        M = []
        for fidx, E in enumerate(sim.E):
            r0_P, r0_M = get_center_of_energydensity(sim_p, fidx)
            
            # wl, spec = tools.calculate_spectrum(sim_p, fidx, linear_py.multipole_decomp)
            p,m,pq,mq = linear_py.multipole_decomp(sim_p, fidx)
        
            P.append([E[0], p, dict(r0=r0, r0_P=r0_P)])
            M.append([E[0], m, dict(r0=r0, r0_M=r0_M)])
    
        sim_p.P = P
        sim_p.M = M
        
        
        P_M_dict = dict(P=P, M=M, r0=r0, geo=sim_p.struct.geometry,
                        wavelengths=wavelengths, k0_spectrum=k0_spectrum,
                        sim=sim_p)
        dict_P_M_list.append(P_M_dict)
    
    
    
    if return_full_sim:
        if return_dressed_simulations:
            return P_M_dict, sim, dressed_simulations
        else:
            return P_M_dict, sim
    else:
        if return_dressed_simulations:
            return P_M_dict, dressed_simulations
        else:
            return P_M_dict
    




def get_dressed_pseudo_polarizability(sim, sub_geo_list=None, 
                                      method='lu', matrix_setup='numba', 
                                      return_full_sim=False,
                                      return_dressed_simulations=False,
                                      verbose=0):
    from pyGDM2 import fields
    from pyGDM2 import core
    from pyGDM2 import linear_py
    from pyGDM2 import tools
    
    if sub_geo_list is not None:
        sub_geo_list = np.array(sub_geo_list)
        if len(sub_geo_list.shape) == 2:
            sub_geo_list = sub_geo_list[None,:,:]   # add an additional first dimension
        if sub_geo_list.shape[2] != 3:
            raise Exception("geo split list must be composed of (x,y,z) coordinate tuples!")
    else: 
        sub_geo_list = sim.struct.geometry.copy()[None,:,:]
    
    
    field_generator = fields.evanescent_planewave
    ## +180: ||-z, +90: ||-x
    kwargs = dict(theta_inc=[180.0, 90], polar=['s', 'p'])
    
    wavelengths = sim.efield.wavelengths
    efield = fields.efield(field_generator, 
                           wavelengths=wavelengths, kwargs=kwargs)
    
    ## create new simulation object
    sim_full = core.simulation(sim.struct, efield)
    
    
    ## --- run the FULL scattering simulation ---
    if verbose:
        print(" --- run full simulation ---")
    sim_full.scatter(method=method, matrix_setup=matrix_setup, verbose=verbose)

    dict_pola_list = []
    dressed_simulations = []
    for i_subgeo, geo_split in enumerate(sub_geo_list):
        if verbose:
            print("   sub-geometry {}/{}".format(i_subgeo+1, len(sub_geo_list)))
        ## use fraction of geometry for polarizability calculation:
        sim_p, _sim_rest = tools.split_simulation(sim_full, geo_split)
        if return_dressed_simulations:
            dressed_simulations.append(copy.deepcopy(sim_p))
        
        r0 = np.average(sim_p.struct.geometry, axis=0)
        
        ## change this to energy density!
        r0_ED = r0
        r0_MD = r0
        
        alpha_shape = (len(wavelengths), 3, 3)
        alpha_dummy = np.zeros(alpha_shape, dtype=np.complex)
        
        a_EE_x_dc = np.zeros(alpha_shape, dtype=np.complex)
        a_HE_x_dc = np.zeros(alpha_shape, dtype=np.complex)
        
        a_EE_z_dc = np.zeros(alpha_shape, dtype=np.complex)
        a_HE_z_dc = np.zeros(alpha_shape, dtype=np.complex)
        
        
        # =============================================================================
        # k_x (fidx 1 and 3)
        # =============================================================================
        ## 1: inc 90(X) - pol s(y)   --> M||z  (x-yz)  --> P||Y  (x-yy)
        ## 3: inc 90(X) - pol p(z)   --> M||y  (x-zy)  --> P||Z  (x-zz)
        fidx = 1
        wl, spec = tools.calculate_spectrum(sim_p, fidx, linear_py.multipole_decomp)
        p_kx_py = np.array([s[0] for s in spec])  # s[0]: electric dipole moment
        m_kx_py = np.array([s[1] for s in spec])  # s[1]: magnetic dipole moment
        a_EE_x_dc[:,1,:] = p_kx_py
        a_HE_x_dc[:,1,:] = m_kx_py
        
        fidx = 3
        wl, spec = tools.calculate_spectrum(sim_p, fidx, linear_py.multipole_decomp)
        p_kx_pz = np.array([s[0] for s in spec])  # s[0]: electric dipole moment
        m_kx_pz = np.array([s[1] for s in spec])  # s[1]: magnetic dipole moment
        a_EE_x_dc[:,2,:] = p_kx_pz
        a_HE_x_dc[:,2,:] = m_kx_pz
        
        
        # =============================================================================
        # k_z (fidx 0 and 2)
        # =============================================================================
        ## 0: inc 0(Z)  - pol s(y)   --> M||x  (z-yx)  --> P||Y  (z-yy)
        ## 2: inc 0(Z)  - pol p(x)   --> M||y  (z-xy)  --> P||X  (z-xx)
        fidx = 0
        wl, spec = tools.calculate_spectrum(sim_p, fidx, linear_py.multipole_decomp)
        p_kz_py = np.array([s[0] for s in spec])  # s[0]: electric dipole moment
        m_kz_py = np.array([s[1] for s in spec])  # s[1]: magnetic dipole moment
        a_EE_z_dc[:,1,:] = p_kz_py
        a_HE_z_dc[:,1,:] = m_kz_py
        
        fidx = 2
        wl, spec = tools.calculate_spectrum(sim_p, fidx, linear_py.multipole_decomp)
        p_kz_px = np.array([s[0] for s in spec])  # s[0]: electric dipole moment
        m_kz_px = np.array([s[1] for s in spec])  # s[1]: magnetic dipole moment
        a_EE_z_dc[:,0,:] = p_kz_px
        a_HE_z_dc[:,0,:] = m_kz_px
        
        
        aee_phase = np.moveaxis([a_EE_x_dc, alpha_dummy, a_EE_z_dc], 0,1)
        ahe_phase = np.moveaxis([a_HE_x_dc, alpha_dummy, a_HE_z_dc], 0,1)
        
        k0_spectrum = 2 * np.pi / np.array(wavelengths).copy()
        
    
        dict_pola = dict(
                        sim=sim, r0=r0, r0_ED=r0_ED, r0_MD=r0_MD,
                        alpha_EE=alpha_dummy, alpha_HE=alpha_dummy, 
                        wavelengths=sim.efield.wavelengths, 
                        alpha_EE_phase=aee_phase, alpha_HE_phase=ahe_phase,
                        k0_spectrum=k0_spectrum,
                         )
        dict_pola_list.append(dict_pola)
    
    
    if return_full_sim:
        if return_dressed_simulations:
            return dict_pola_list, sim_full, dressed_simulations
        else:
            return dict_pola_list, sim_full
    else:
        if return_dressed_simulations:
            return dict_pola_list, dressed_simulations
        else:
            return dict_pola_list
    



# =============================================================================
# extract dressed pseudo-polarizability VERSION GEN. PROPAGATOR (with plane wave phase term)
# =============================================================================
def get_dressed_ps_pola(sim, sub_geo_list=None, 
                        method='scipyinv',
                        return_dressed_simulations=False,
                        verbose=1):
    import time
    from pyGDM2.core import get_general_propagator
    from pyGDM2 import tools
    if verbose:
        print("Note: field generator will be ignored in the effective polarizability calculation.")
    
    if sub_geo_list is not None:
        sub_geo_list = np.array(sub_geo_list)
        if len(sub_geo_list.shape) == 2:
            sub_geo_list = sub_geo_list[None,:,:]   # add an additional first dimension
        if sub_geo_list.shape[2] != 3:
            raise Exception("geo split list must be composed of (x,y,z) coordinate tuples!")
    else: 
        sub_geo_list = sim.struct.geometry.copy()[None,:,:]
    
    
    ## ---- get meshpoint indices of sub-geometry for correct generalized-propagator integration
    idx_split_list = []
    r0_list = []
    dressed_simulations = []
    for i_subgeo, geo_split in enumerate(sub_geo_list):
        
        sim_p, _sim_rest = tools.split_simulation(sim, geo_split)
        dressed_simulations.append(copy.deepcopy(sim_p))
        
        geo = sim.struct.geometry
        idx_split = []
        for i, pos in enumerate(geo):
            ## test if 'pos' exists in geometry
            if np.linalg.norm(geo_split - pos, axis=1).min() < sim.struct.step/4. :
                idx_split.append(i)
        idx_split_list.append(np.array(idx_split))
    
        r0 = np.average(geo[idx_split], axis=0)
        r0_list.append(r0)
        
    

    ## ---- iterate over wavelengths
    aee = np.zeros( (len(idx_split_list), len(sim.efield.wavelengths), 3,3), dtype=np.complex )
    ahe = np.zeros( (len(idx_split_list), len(sim.efield.wavelengths), 3,3), dtype=np.complex )
    aee_phase = np.zeros( (len(idx_split_list), len(sim.efield.wavelengths), 3, 3,3), dtype=np.complex )
    ahe_phase = np.zeros( (len(idx_split_list), len(sim.efield.wavelengths), 3, 3,3), dtype=np.complex )
            
    
    for i_wl, wavelength in enumerate(sim.efield.wavelengths):
        
        if verbose: 
            t0=time.time()
            print("Wavelength: {}nm".format(wavelength))
        
        ## --- evaluate polarizabilities and normalization terms at "wavelength"
        sim.dyads.getEnvironmentIndices(wavelength, sim.struct.geometry)
        sim.struct.getNormalization(wavelength)
        alpha = sim.dyads.getPolarizabilityTensor(wavelength, sim.struct.geometry)
        
        
        ## !!!!!!!!!!!!!!!!!! ----- TODO: ADAPT TO NEW PYGDM API
        ## !!!!!!!!!!!!!!!!!! ----- TODO: ADAPT TO NEW PYGDM API
        ## !!!!!!!!!!!!!!!!!! ----- TODO: ADAPT TO NEW PYGDM API
        ## !!!!!!!!!!!!!!!!!! ----- TODO: ADAPT TO NEW PYGDM API
        ## !!!!!!!!!!!!!!!!!! ----- TODO: ADAPT TO NEW PYGDM API
        ## wavenr
        k0 = sim.struct.n2 * 2*np.pi / wavelength
        if verbose:
            t0=time.time()
            print("Wavelength: {}nm".format(wavelength))
        
        
        ## --- General Propagator: Inversion of CAM0
        K = get_general_propagator(sim, wavelength=wavelength, method=method)
        
        ## --- get explicit matrix
        if method.lower() == 'lu':
            import scipy.linalg as la
            K = la.lu_solve(K, np.identity(3*len(sim.struct.geometry)))
        
        ## convert to matrix of 3x3 submatrices coupling dp i and j [ K(r_i, r_j, omega) ]
        if method.lower() in ['cuda', 'cupy']:
            K = K.get()
        if verbose:
            print("         inversion: {:.3f}s, ".format(time.time()-t0), end='')
            t0=time.time()
            
        ## reshape K to be array of 3x3 tensors
        # K = K[idx_select_K][:,idx_select_K]
        K2 = K.reshape(len(K)//3, 3, -1, 3).swapaxes(1,2).reshape(-1, 3, 3).reshape(len(K)//3,len(K)//3, 3, 3)
        ## use part of K which corresponds to sub-structure
        # K2 = K2[idx_split]#[:,idx_split]
        
        
        ## iterate over geometries
        for i_subgeo, idx_split in enumerate(idx_split_list):
            r0 = r0_list[i_subgeo]
            Dr = geo - r0
            
            ## phase factor: scalar product k*r
            kr_x = np.sum(np.multiply(np.array([-k0, 0, 0]), Dr), axis=1)
            kr_y = np.sum(np.multiply(np.array([0, -k0, 0]), Dr), axis=1)
            kr_z = np.sum(np.multiply(np.array([0, 0, -k0]), Dr), axis=1)
            eikr_x = np.exp(1j * kr_x)
            eikr_y = np.exp(1j * kr_y)
            eikr_z = np.exp(1j * kr_z)
            
        ## ----------     EE
            t0 = time.time()
            K2_a_ee = np.prod([alpha[None,:,None,None], K2])
            alpha_ee0 = np.sum(K2_a_ee, axis=(1,0))
            
            ## pseudo-polarizabilities
            alpha_ee_ph_x = np.sum(np.sum(K2_a_ee * eikr_x[:,None,None,None], axis=0)[idx_split], axis=0)
            alpha_ee_ph_y = np.sum(np.sum(K2_a_ee * eikr_y[:,None,None,None], axis=0)[idx_split], axis=0)
            alpha_ee_ph_z = np.sum(np.sum(K2_a_ee * eikr_z[:,None,None,None], axis=0)[idx_split], axis=0)
            
            alpha_ee_phase = np.array([alpha_ee_ph_x, alpha_ee_ph_y, alpha_ee_ph_z])
            if verbose: print("calc. EE: {:.3f}s, ".format(time.time()-t0), end='')
            
            
        ## ----------     EH
            t0 = time.time()
            K2_a_he = np.cross(Dr[None,:,None], K2_a_ee)   # r cross K
            
            ik2 = -(1j*k0/2.)
            alpha_he0 =  ik2 * np.sum(K2_a_he, axis=(1,0))  # i*k/2 * sum
            
            ## pseudo-polarizabilities
            alpha_he_ph_x = (ik2 * np.sum(np.sum(K2_a_he * eikr_x[:,None,None,None], axis=0)[idx_split], axis=0))
            alpha_he_ph_y = (ik2 * np.sum(np.sum(K2_a_he * eikr_y[:,None,None,None], axis=0)[idx_split], axis=0))
            alpha_he_ph_z = (ik2 * np.sum(np.sum(K2_a_he * eikr_z[:,None,None,None], axis=0)[idx_split], axis=0))
            
            alpha_he_phase = np.array([alpha_he_ph_x, alpha_he_ph_y, alpha_he_ph_z])
            
            if verbose: print("calc. HE: {:.3f}s".format(time.time()-t0))
            
            
            aee[i_subgeo, i_wl] = alpha_ee0
            ahe[i_subgeo, i_wl] = alpha_he0
            
            aee_phase[i_subgeo, i_wl] = alpha_ee_phase            
            ahe_phase[i_subgeo, i_wl] = alpha_he_phase
        
        
        
    ## assemble dictionaries
    dict_pola_list = []
    for i_subgeo, sim_p in enumerate(dressed_simulations):
        k0_spectrum = 2 * np.pi / np.array(sim.efield.wavelengths).copy()
        r0 = r0_list[i_subgeo]
        dict_pola = dict(
                        sim=sim, sim_p=sim_p,
                        r0=r0, r0_ED=r0, r0_MD=r0, 
                        wavelengths=sim.efield.wavelengths, 
                        k0_spectrum=k0_spectrum,
                        alpha_EE=aee[i_subgeo], alpha_HE=ahe[i_subgeo], 
                        alpha_EE_phase=aee_phase[i_subgeo], alpha_HE_phase=ahe_phase[i_subgeo],
                         )
            
        dict_pola_list.append(dict_pola)
    

    if return_dressed_simulations:
        return dict_pola_list, dressed_simulations
    else:
        return dict_pola_list






# =============================================================================
# extract dressed (static!) polarizability
# =============================================================================
def get_dressed_eff_polarizability(sim, sub_geo_list, method, verbose=1):
    """
    """
    from pyGDM2 import core
    if type(sub_geo_list[0]) == core.simulation:
        geo_list = [_s.struct.geometry for _s in sub_geo_list]
    else:
        geo_list = sub_geo_list
    
    
    from pyGDM2.core import get_general_propagator, simulation
    from pyGDM2 import tools
    
# =============================================================================
#     exception handling
# =============================================================================
    if method.lower() =='lu':
        warnings.warn("'LU' is not efficient for polarizability extraction. Consider using `scipyinv` instead.")
        
    if verbose:
        print("Note: field generator will be ignored in the effective polarizability calculation.")
    
    if type(sub_geo_list[0]) == simulation:
        geo_list = []
        for _sim in sub_geo_list:
            geo_list.append(_sim.struct.geometry)
    else:
        geo_list = sub_geo_list
    
# =============================================================================
#     extract sub-geometry ("dressed" part of simulation)
# =============================================================================
    ## ---- get meshpoint indices of sub-geometry for correct generalized-propagator integration
    idx_split_list = []
    r0_list = []
    dressed_simulations = []
    for i_subgeo, geo_split in enumerate(geo_list):
        
        sim_p, _sim_rest = tools.split_simulation(sim, geo_split)
        dressed_simulations.append(copy.deepcopy(sim_p))
        
        geo = sim.struct.geometry
        idx_split = []
        for i, pos in enumerate(geo):
            ## test if 'pos' exists in geometry
            if np.linalg.norm(geo_split - pos, axis=1).min() < sim.struct.step/4. :
                idx_split.append(i)
        idx_split_list.append(np.array(idx_split))
    
        r0 = np.average(geo[idx_split], axis=0)
        r0_list.append(r0)
    
# =============================================================================
#     calculate dressed pola for each sub-structure
# =============================================================================
    ## ---- iterate over sub-structures and over wavelengths
    aee = np.zeros( (len(idx_split_list), len(sim.efield.wavelengths), 3,3), dtype=np.complex )
    
    ##### -----------------------
    for i_wl, wavelength in enumerate(sim.efield.wavelengths):
        
        if verbose: 
            t0=time.time()
            print("timing for wl={}nm - ".format(wavelength), end='')
        
        ## --- General Propagator: Inversion of CAM0
        K = get_general_propagator(sim, wavelength=wavelength, method=method, verbose=verbose)
        
        ## --- get explicit matrix
        if method.lower() == 'lu':
            import scipy.linalg as la
            K = la.lu_solve(K, np.identity(3*len(sim.struct.geometry)))        
        if method.lower() in ['cuda', 'cupy']:
            K = K.get()
            if verbose:
                t0 = time.time()
                print('GPU-->RAM: {:.3f}ms, '.format(1000*(time.time()-t0)), end='') 
        if verbose:
            print(" inversion: {:.3f}s, ".format(time.time()-t0), end='')
            t0=time.time()
        
        
        ## convert to matrix of 3x3 submatrices coupling dp i and j [ K(r_i, r_j, omega) ]
        ## reshape K to be array of 3x3 tensors
        K2 = K.reshape(len(K)//3, 3, -1, 3).swapaxes(1,2).reshape(-1, 3, 3).reshape(len(K)//3,len(K)//3, 3, 3)
        ## use part of K which corresponds to sub-structure
        # K2 = K2[idx_split]#[:,idx_split]
        
        ## polarizability of full structure
        alpha = sim.dyads.getPolarizabilityTensor(wavelength, sim.struct)
        
        
        ## ---------- extract EE
        t0 = time.time()
        K2_a_ee = np.matmul(alpha, K2)
        
        ## iterate over multiple sub-geometries (if given)
        for i_subgeo, idx_split in enumerate(idx_split_list):
            ## use part of "alpha.K2" which corresponds to sub-structure            
            alpha_ee0 = np.sum(K2_a_ee[:,idx_split], axis=(1,0))    
            aee[i_subgeo, i_wl] = alpha_ee0
            
    if verbose:
        print('extract alpha: {:.1f}ms.'.format(1000*(time.time()-t0)))
    
    ## assemble dictionaries
    dict_pola_list = []
    for i_subgeo, sim_p in enumerate(dressed_simulations):
        r0 = r0_list[i_subgeo]
        dict_pola = dict(
                        sim=sim, sim_p=sim_p,
                        r0=r0, 
                        wavelengths=sim.efield.wavelengths, 
                        alpha_EE=aee[i_subgeo],
                        alpha_HE=aee[i_subgeo]*0  # set magnetic HE pola to zero
                         )
            
        dict_pola_list.append(dict_pola)
    
    
    return dict_pola_list



# =============================================================================
# extract dressed (static!) polarizability
# =============================================================================
def get_dipole_transfer_matrix(sim, r_dp, method='lu', verbose=1):
    """
    r_dp: 3-vector
        position of emitting dipole origin
    """    
    from pyGDM2.core import get_general_propagator, simulation
    from pyGDM2 import tools
    
# =============================================================================
#     exception handling
# =============================================================================
    if verbose:
        print("Note: field generator will be ignored in the transfer matrix calculation.")
    
# =============================================================================
#     extract sub-geometry ("dressed" part of simulation)
# =============================================================================
    ## ---- get meshpoint indices of sub-geometry for correct generalized-propagator integration
    idx_split_list = []
    r0_list = []
    dressed_simulations = []
    for i_subgeo, geo_split in enumerate(geo_list):
        
        sim_p, _sim_rest = tools.split_simulation(sim, geo_split)
        dressed_simulations.append(copy.deepcopy(sim_p))
        
        geo = sim.struct.geometry
        idx_split = []
        for i, pos in enumerate(geo):
            ## test if 'pos' exists in geometry
            if np.linalg.norm(geo_split - pos, axis=1).min() < sim.struct.step/4. :
                idx_split.append(i)
        idx_split_list.append(np.array(idx_split))
    
        r0 = np.average(geo[idx_split], axis=0)
        r0_list.append(r0)
    
# =============================================================================
#     calculate dressed pola for each sub-structure
# =============================================================================
    ## ---- iterate over sub-structures and over wavelengths
    aee = np.zeros( (len(idx_split_list), len(sim.efield.wavelengths), 3,3), dtype=np.complex )
    
    ##### -----------------------
    for i_wl, wavelength in enumerate(sim.efield.wavelengths):
        
        if verbose: 
            t0=time.time()
            print("timing for wl={}nm - ".format(wavelength), end='')
        
        ## --- General Propagator: Inversion of CAM0
        K = get_general_propagator(sim, wavelength=wavelength, method=method, verbose=verbose)
        
        ## --- get explicit matrix
        if method.lower() == 'lu':
            import scipy.linalg as la
            K = la.lu_solve(K, np.identity(3*len(sim.struct.geometry)))        
        if method.lower() in ['cuda', 'cupy']:
            K = K.get()
            if verbose:
                t0 = time.time()
                print('GPU-->RAM: {:.3f}ms, '.format(1000*(time.time()-t0)), end='') 
        if verbose:
            print(" inversion: {:.3f}s, ".format(time.time()-t0), end='')
            t0=time.time()
        
        
        ## convert to matrix of 3x3 submatrices coupling dp i and j [ K(r_i, r_j, omega) ]
        ## reshape K to be array of 3x3 tensors
        K2 = K.reshape(len(K)//3, 3, -1, 3).swapaxes(1,2).reshape(-1, 3, 3).reshape(len(K)//3,len(K)//3, 3, 3)
        ## use part of K which corresponds to sub-structure
        # K2 = K2[idx_split]#[:,idx_split]
        
        ## polarizability of full structure
        alpha = sim.dyads.getPolarizabilityTensor(wavelength, sim.struct)
        
        
        ## ---------- extract EE
        t0 = time.time()
        K2_a_ee = np.matmul(alpha, K2)
        
        ## iterate over multiple sub-geometries (if given)
        for i_subgeo, idx_split in enumerate(idx_split_list):
            ## use part of "alpha.K2" which corresponds to sub-structure            
            alpha_ee0 = np.sum(K2_a_ee[:,idx_split], axis=(1,0))    
            aee[i_subgeo, i_wl] = alpha_ee0
            
    if verbose:
        print('extract alpha: {:.1f}ms.'.format(1000*(time.time()-t0)))
    
    ## assemble dictionaries
    dict_pola_list = []
    for i_subgeo, sim_p in enumerate(dressed_simulations):
        r0 = r0_list[i_subgeo]
        dict_pola = dict(
                        sim=sim, sim_p=sim_p,
                        r0=r0, 
                        wavelengths=sim.efield.wavelengths, 
                        alpha_EE=aee[i_subgeo],
                        alpha_HE=aee[i_subgeo]*0  # set magnetic HE pola to zero
                         )
            
        dict_pola_list.append(dict_pola)
    
    
    return dict_pola_list






















# =============================================================================
# use polarizabilities
# =============================================================================
def illuminate_alphaEE_static(alpha_list, field_gen, env_dict, field_kwargs):
    """illuminate static polarizabilities and calculate electric dipole moments
    
    Parameters
    ----------
    alpha_list : list of dict
        list of polarizability-dictionaries
    
    field_gen : field generator function (from pyGDM2.fields)
        illuminating field 
    
    env_dict : dict
        dictionary defining the environment. Contains: eps1, eps2, eps3 and spacing
    
    field_kwargs : dict
        configuration for illumination field
    
    """

    ED_pos = np.zeros((len(alpha_list[0]['wavelengths']), len(alpha_list), 3), dtype=np.float32)
    ED_moments = np.zeros((len(alpha_list[0]['wavelengths']), len(alpha_list), 3), dtype=np.complex64)
    E0_all = np.zeros((len(alpha_list[0]['wavelengths']), len(alpha_list), 3), dtype=np.complex64)
    for i_element, a_di in enumerate(alpha_list):
        pos = np.array([a_di['r0']])
        for i_wl, wavelength in enumerate(a_di['wavelengths']):
            E0 = field_gen(pos, env_dict, wavelength, **field_kwargs)
            a = a_di['alpha_EE'][i_wl]
            _P = np.dot(a, E0[0])
            ED_pos[i_wl, i_element] = a_di['r0']
            ED_moments[i_wl, i_element] = _P
            E0_all[i_wl, i_element] = E0[0]
    
    return ED_pos, ED_moments, E0_all




def repropagate_ED_nf(r_ed, ed, sim, wavelength, r_probe,
                      eps1=1, eps2=1, eps3=1, spacing=5000):
    """repropagate dipole moments to nearfield calculation

    """
    from pyGDM2 import linear
    
    conf_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
    # conf_dict['eps1'] = conf_dict['eps2'] 
    Gp = np.zeros((len(r_ed), len(r_probe), 3, 3), dtype = sim.efield.dtypec)
    sim.dyads.eval_G(r_ed, r_probe, sim.dyads.G_EE, wavelength, conf_dict, Gp)
    Gp = np.moveaxis(Gp, 0,2).reshape((len(r_probe), 3, -1))  # re-arrange for direct matrix-vector multiplication
    
    ## propagate fields 
    Escat = np.zeros(shape=(len(r_probe), 3), dtype=sim.efield.dtypec)
    linear._calc_repropagation(ed, Escat, Gp)
    
    return np.concatenate([r_probe, Escat], axis=-1)


def repropagate_ED_nf_magnetic(r_ed, ed, sim, wavelength, r_probe,
                      eps1=1, eps2=1, eps3=1, spacing=5000):
    """repropagate dipole moments for magnetic nearfield calculation

    """
    from pyGDM2 import linear
    
    conf_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
    # conf_dict['eps1'] = conf_dict['eps2'] 
    Gp = np.zeros((len(r_ed), len(r_probe), 3, 3), dtype = sim.efield.dtypec)
    sim.dyads.eval_G(r_ed, r_probe, sim.dyads.G_HE, wavelength, conf_dict, Gp)
    Gp = np.moveaxis(Gp, 0,2).reshape((len(r_probe), 3, -1))  # re-arrange for direct matrix-vector multiplication
    
    ## propagate fields 
    Hscat = np.zeros(shape=(len(r_probe), 3), dtype=sim.efield.dtypec)
    linear._calc_repropagation(ed, Hscat, Gp)
    
    return np.concatenate([r_probe, Hscat], axis=-1)


def repropagate_ED_ff(r_ed, ed, sim, wavelength, conf_dict,
                      # eps1=1, eps2=1, eps3=1, spacing=5000,
                      r=100000, 
                      tetamin=0, tetamax=np.pi, Nteta=36, 
                      phimin=0, phimax=0, Nphi=1):
    """repropagate dipole moments to farfield calculation

    """
    from pyGDM2 import linear
    
    
    ## spherical --> cartesian coordinates
    tetalist = np.ones((Nteta, Nphi))*np.linspace(tetamin, tetamax, Nteta)[:,None]
    philist = np.ones((Nteta, Nphi))*np.linspace(phimin, phimax, Nphi, endpoint=False)[None,:]
    xff = (r * np.sin(tetalist) * np.cos(philist)).flatten()
    yff = (r * np.sin(tetalist) * np.sin(philist)).flatten()
    zff = (r * np.cos(tetalist)).flatten()
    r_probe = np.array([xff, yff, zff]).astype(np.float32).T
    
    # ## differentials for integration
    # dteta = (tetamax-tetamin)/float(Nteta-1)
    # dphi = 2.*np.pi/float(Nphi)
    
    # conf_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
    # conf_dict = dict(eps1=np.complex64(eps1), eps2=np.complex64(eps2), eps3=np.complex64(eps3), spacing=np.complex64(spacing))
    
    Gp = np.zeros((len(r_ed), len(r_probe), 3, 3), dtype = sim.efield.dtypec)
    sim.dyads.eval_G(r_ed, r_probe, sim.dyads.G_EE_ff, wavelength, conf_dict, Gp)
    Gp = np.moveaxis(Gp, 0,2).reshape((len(r_probe), 3, -1))  # re-arrange for direct matrix-vector multiplication
    
    ## propagate fields 
    Escat = np.zeros(shape=(len(r_probe), 3), dtype=sim.efield.dtypec)
    linear._calc_repropagation(ed, Escat, Gp)
    
    EF_dp = np.concatenate([r_probe, Escat], axis=-1)
    I_s = np.sum(np.abs(Escat)**2, axis=-1)
    
    return tetalist, philist, I_s, EF_dp
    

###---------------------------------
###---------------------------------
    # from pyGDM2 import linear
    
    # ## spherical --> cartesian coordinates
    # tetalist = np.ones((Nteta, Nphi))*np.linspace(tetamin, tetamax, Nteta)[:,None]
    # philist = np.ones((Nteta, Nphi))*np.linspace(phimin, phimax, Nphi, endpoint=False)[None,:]
    # xff = (r * np.sin(tetalist) * np.cos(philist)).flatten()
    # yff = (r * np.sin(tetalist) * np.sin(philist)).flatten()
    # zff = (r * np.cos(tetalist)).flatten()
    # r_probe = np.array([xff, yff, zff]).astype(np.float32).T
    
    # ## differentials for integration
    # dteta = (tetamax-tetamin)/float(Nteta-1)
    # dphi = 2.*np.pi/float(Nphi)
    
    # conf_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
    
    # Gp = np.zeros((len(r_ed), len(r_probe), 3, 3), dtype = sim.efield.dtypec)
    # Gm = np.zeros((len(r_md), len(r_probe), 3, 3), dtype = sim.efield.dtypec)
    
    # sim.dyads.eval_G(r_ed, r_probe, sim.dyads.G_EE, wavelength, conf_dict, Gp)
    # Gp = np.moveaxis(Gp, 0,2).reshape((len(r_probe), 3, -1))  # re-arrange for direct matrix-vector multiplication
    # sim.dyads.eval_G(r_md, r_probe, sim.dyads.G_HE, wavelength, conf_dict, Gm)
    # Gm = np.moveaxis(Gm, 0,2).reshape((len(r_probe), 3, -1))  # re-arrange for direct matrix-vector multiplication
    
    # ## propagate fields 
    # Escat_p = np.zeros(shape=(len(r_probe), 3), dtype=sim.efield.dtypec)
    # Escat_m = np.zeros(shape=(len(r_probe), 3), dtype=sim.efield.dtypec)
    # linear._calc_repropagation(ed, Escat_p, Gp)
    # linear._calc_repropagation(md, Escat_m, Gm)
    # Escat = (Escat_p + Escat_m)
    # # Escat = Escat_p
    # # Escat = Escat_m
    
    # NF_dp = np.concatenate([r_probe, Escat], axis=-1)
    # I_s = np.sum(np.abs(Escat)**2, axis=-1)
    
    # return tetalist, philist, I_s, NF_dp
###---------------------------------
###---------------------------------

def calc_NF_alphaEE_static(ED_pos, ED_moments, sim, wavelength, env_dict,
                           r_probe):
    """calculate nearfield from spatial distribution electric dipole moments

    Parameters
    ----------
    ED_pos : list of 3-tuples
        electric dipole positions
        
    ED_moments : list of 3-tuples
        electric dipole moments
        
    sim : pyGDM2 sim
        simulation, used only for config
        
    wavelength : float
        wavelength
    
    env_dict : dict
        dictionary defining the environment. Contains: eps1, eps2, eps3 and spacing
    
    r_probe : list of 3-tuples
        list of coordinates to calculate nearfield

    Returns
    -------
    E_scat, E_tot, E0

    """
    from pyGDM2 import linear
    
    E_scat = repropagate_ED_nf(ED_pos, ED_moments, sim, 
                               wavelength=wavelength, r_probe=r_probe, 
                               **env_dict)
    
    ## calc incident field to get full field
    E0 = linear.nearfield(sim, 0, r_probe, which_fields=["E0"])[0] # 'sim' serves only for field-generator config.
    E_tot = E_scat.copy()
    E_tot[:,3:] += E0[:,3:]
    
    return E_scat, E_tot, E0

def calc_NF_magnetic_alphaEE_static(ED_pos, ED_moments, sim, wavelength, env_dict,
                           r_probe):
    """calculate magnetic nearfield from spatial distribution electric dipole moments

    Parameters
    ----------
    ED_pos : list of 3-tuples
        electric dipole positions
        
    ED_moments : list of 3-tuples
        electric dipole moments
        
    sim : pyGDM2 sim
        simulation, used only for config
        
    wavelength : float
        wavelength
    
    env_dict : dict
        dictionary defining the environment. Contains: eps1, eps2, eps3 and spacing
    
    r_probe : list of 3-tuples
        list of coordinates to calculate nearfield

    Returns
    -------
    E_scat, E_tot, E0

    """
    from pyGDM2 import linear
    
    H_scat = repropagate_ED_nf_magnetic(ED_pos, ED_moments, sim, 
                               wavelength=wavelength, r_probe=r_probe, 
                               **env_dict)
    
    ## calc incident field to get full field
    H0 = linear.nearfield(sim, 0, r_probe, which_fields=["H0"])[0] # 'sim' serves only for field-generator config.
    H_tot = H_scat.copy()
    H_tot[:,3:] += H0[:,3:]
    
    return H_scat, H_tot, H0



def calc_FF_alphaEE_static(ED_pos, ED_moments, sim, wavelength, env_dict,
                           r=100000, 
                           tetamin=0, tetamax=np.pi, Nteta=36, 
                           phimin=0, phimax=2*np.pi, Nphi=36):
    """integrate farfield scattering towards a specified solid angle

    Parameters
    ----------
    ED_pos : list of 3-tuples
        electric dipole positions
        
    ED_moments : list of 3-tuples
        electric dipole moments
        
    sim : pyGDM2 sim
        simulation, used only for config
        
    wavelength : float
        wavelength
    
    env_dict : dict
        dictionary defining the environment. Contains: eps1, eps2, eps3 and spacing
    
    r=100000, tetamin=0, tetamax=np.pi, Nteta=36, phimin=0, phimax=2*np.pi, Nphi=36 : 
        definition of integration solid angle

    Returns
    -------
    E_scat, E_tot, E0

    """
    tetalist, philist, I_s, EF_dp = repropagate_ED_ff(
            ED_pos, ED_moments, sim, wavelength, env_dict, 
            r=r, tetamin=tetamin, tetamax=tetamax, Nteta=Nteta, 
            phimin=phimin, phimax=phimax, Nphi=Nphi)
    
    ## integrate field intensity
    dteta = (tetamax-tetamin) / float(Nteta-1)
    dphi = 2.*np.pi / float(Nphi)
    d_solid_surf = r**2 * np.sin(tetalist) * dteta * dphi
    I_integrated = np.sum(I_s * d_solid_surf.flatten())
    
    return I_integrated