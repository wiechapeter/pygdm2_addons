# encoding: utf-8
#
#Copyright (C) 2021, P. R. Wiecha
#
#last edited: 12 January 2021
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
polarizability extraction and re-coupling tools for pyGDM2
"""
from __future__ import print_function, division

import copy
import time
import warnings

import numpy as np

from pyGDM2 import structures




# =============================================================================
# center of mass / center of energy density
# =============================================================================
def get_center_of_mass_list(geo_list):
    r0_list = []
    for g in geo_list:
        r0_list.append(np.average(g, axis=0))
    
    return np.array(r0_list)


def get_center_of_energydensity(sim, field_index, N_iter_r0_MD=3):
        
    field_params = sim.E[field_index][0]
    wavelength = field_params['wavelength']
    k0 = 2*np.pi / wavelength
    
    geo = sim.struct.geometry
    E = sim.E[field_index][1]
    
# =============================================================================
#     polarization center of energy
# =============================================================================
    ##     P = sum_i p(r_i) = chi V_cell sum_i E(r_i)
    alpha_tensor = sim.dyads.getPolarizabilityTensor(wavelength, sim.struct)
    P = np.matmul(alpha_tensor, E[...,None])[...,0]
    p_tot_abs = np.sum(P, axis=0)
    
    
    
    ## scalar projection on the total electric moment vector
    proj_p_scalar = np.dot(P, p_tot_abs) / np.linalg.norm(p_tot_abs)
    proj_p_energy_dens = np.abs(proj_p_scalar)**2
    center_loc_P = np.average(geo, axis=0, weights=proj_p_energy_dens)
    center_loc_P = center_loc_P.real


# =============================================================================
#     magnetization center of energy
# =============================================================================
    ## iterative determination, starting point is center of mass
    ## --- use center of gravity for multimode expansion
    center_loc_M = np.average(geo, axis=0)
    
    for i in range(N_iter_r0_MD*2 - 1):
        Dr = geo - center_loc_M
        M = -(1j*k0/2.) * np.cross(Dr, P)
        m_tot_abs = np.sum(M, axis=0)
        
        ## scalar projection on the total magnetic moment vector
        proj_m_scalar = np.dot(M, m_tot_abs) / np.linalg.norm(m_tot_abs)
        proj_m_energy_dens = np.abs(proj_m_scalar)**2
        center_loc_M = np.average(geo, axis=0, weights=proj_m_energy_dens)
        center_loc_M = center_loc_M.real

    return center_loc_P, center_loc_M


# =============================================================================
# ED / MD dipole moments in simulation
# =============================================================================
def decomp_ed_md(sim, field_index, r0='center_of_mass'):
    """ED/MD decomposition of nanostructure optical response
    
    electric / magnetic dipole moments from field inside nanostructure


    Parameters
    ----------
    sim : :class:`.core.simulation`
        simulation description
    
    field_index : int
        index of evaluated self-consistent field to use for calculation. Can be
        obtained for specific parameter-set using :func:`.tools.get_closest_field_index`
    
    r0 : str, default: None
        'center_of_mass': use structure's center of gravity
        'center_of_energy': use energy density center (iteratively obtained)
    
    
    Returns
    -------
    
    p : 3-vector
        electric dipole moment
    m : 3-vector
        magnetic dipole moment
    r_p : 3-vector
        position of ED
    r_m : 3-vector
        position of MD
    
    Notes
    -----
    For details about the method, see: 
    Evlyukhin, A. B. et al. *Multipole analysis of light scattering by 
    arbitrary-shaped nanoparticles on a plane surface.*, 
    JOSA B 30, 2589 (2013)
    """
    field_params = sim.E[field_index][0]
    wavelength = field_params['wavelength']
    k0 = 2*np.pi / wavelength
    
    geo = sim.struct.geometry
    E = sim.E[field_index][1]
    ex, ey, ez = E.T
    
    ## --- polarizability of each meshpoint
    sim.struct.setWavelength(wavelength)
    alpha_tensor = sim.dyads.getPolarizabilityTensor(wavelength, sim.struct)
    
    ## --- use center of gravity for multimode expansion
    if r0.lower() == 'center_of_mass':
        r0_p = np.average(geo, axis=0)
        r0_m = np.average(geo, axis=0)
    elif r0.lower() == 'center_of_energy':
        r0_p, r0_m = get_center_of_energydensity(sim, field_index, N_iter_r0_MD=3)
    else:
        raise ValueError('unknown r0 determination method')
    
    Dr_m = geo - r0_m
    
    ## --- total electric dipole moment:
    P = np.matmul(alpha_tensor, E[...,None])[...,0]
    p = np.sum(P, axis=0)  # = volume integral
    
    ## --- total magnetic dipole moment: (X=curl)
    M = (1j*k0/2.) * np.cross(Dr_m, P)
    m = np.sum(M, axis=0)

    return p, m, r0_p, r0_m



# =============================================================================
# extract (static!) polarizability 
# =============================================================================
def get_eff_polarizability(sim, method, verbose=1):
    from pyGDM2.core import get_general_propagator
    if method.lower() =='lu':
        warnings.warn("'LU' is not efficient for polarizability extraction. Consider using `scipyinv` instead.")
        
    if verbose:
        print("Note: field generator will be ignored in the effective polarizability calculation.")
    
    geo = sim.struct.geometry
    r0 = np.average(geo, axis=0)   # structure center of gravity
    
    aee0 = []
    for i_wl, wavelength in enumerate(sim.efield.wavelengths):
# ----- calc. generalized propagator K
        K = get_general_propagator(sim, method=method,
                                   wavelength=wavelength, verbose=verbose)
        if verbose: 
            t0 = time.time()
        ## --- get explicit matrix
        if method.lower() == 'lu':
            import scipy.linalg as la
            K = la.lu_solve(K, np.identity(3*len(sim.struct.geometry)))
        if method.lower() in ['cuda', "cupy"]:
            K = K.get()
            if verbose:
                t0 = time.time()
                print('GPU-->RAM: {:.3f}ms, '.format(1000*(time.time()-t0)), end='')
# ----- calc. alpha^EE
        ## reshape K to be array of 3x3 tensors, coupling dp i and j [ K(r_i, r_j, omega) ]
        K2 = K.reshape(len(K)//3, 3, -1, 3).swapaxes(1,2).reshape(-1, 3, 3).reshape(len(K)//3,len(K)//3, 3, 3)
    
        alpha = sim.dyads.getPolarizabilityTensor(wavelength, sim.struct)
        K2_a_ee = np.matmul(alpha, K2)
    
        alpha_ee0 = np.sum(K2_a_ee, axis=(0,1))
        aee0.append(alpha_ee0)
        if verbose:
            print('extract alpha: {:.1f}ms.'.format(1000*(time.time()-t0)))
        
    aee0 = np.array(aee0)
    
    return dict(sim=sim, r0=r0, wavelengths=sim.efield.wavelengths, alpha_EE=aee0)





def get_eff_polarizability_HH(sim_in, method='lu', verbose=1):
    import time
    from pyGDM2.core import get_general_propagator
    from pyGDM2 import fields
    from pyGDM2 import linear
    from pyGDM2 import tools
    if verbose:
        print("Note: field generator will be ignored in the effective polarizability calculation.")
    sim = sim_in.copy()
    wavelengths = sim.efield.wavelengths
    
    field_generator = fields.plane_wave
    field_kwargs = [
        dict(E_s=0, E_p=-1, inc_angle=-90, inc_plane='yz'),  # lin-pol Z, incidence along Y --> H||X
        dict(E_s=0, E_p=1, inc_angle=0, inc_plane='xz'),  # lin-pol X, incidence along Z --> H||Y
        dict(E_s=-1, E_p=0, inc_angle=-90, inc_plane='xz'),  # lin-pol Y, incidence along X --> H||Z
        ]
                    
    # field_kwargs = dict(E_s=0, E_p=1, inc_angle=180, inc_plane='xz') # lin-pol Y, normal incidence

    efield = fields.efield(field_generator, wavelengths=wavelengths, kwargs=copy.deepcopy(field_kwargs))
    
    sim.efield = efield
    sim.scatter(method=method)

    geo = sim.struct.geometry
    
    r0 = np.average(geo, axis=0)
    
    ahh = []
    for i_wl, wl in enumerate(wavelengths):
        if verbose: 
            t0=time.time()
            print("Wavelength: {}nm".format(wl))
        
        t0 = time.time()
        ## --- evaluate polarizabilities and normalization terms at "wavelength"
        idx_Hx = tools.get_closest_field_index(sim, dict(E_s=0, E_p=-1, inc_angle=-90, inc_plane='yz', wavelength=wl))
        idx_Hy = tools.get_closest_field_index(sim, dict(E_s=0, E_p=1, inc_angle=0, inc_plane='xz', wavelength=wl))
        idx_Hz = tools.get_closest_field_index(sim, dict(E_s=-1, E_p=0, inc_angle=-90, inc_plane='xz', wavelength=wl))
        
        p1, m1, _, _ = decomp_ed_md(sim, idx_Hx)
        p2, m2, _, _ = decomp_ed_md(sim, idx_Hy)
        p3, m3, _, _ = decomp_ed_md(sim, idx_Hz)
        # p1, m1 = linear.multipole_decomp(sim, idx_Hx)
        # p2, m2 = linear.multipole_decomp(sim, idx_Hy)
        # p3, m3 = linear.multipole_decomp(sim, idx_Hz)
        
    ## ----------     HH
        ## plane wave mapping E --> H: 
        ##    Hx: ky, Ez
        ##    Hy: kz, Ex
        ##    Hz: kx, Ey
        alpha_hh = np.array([m1,m2,m3]).T
        
        if verbose: print("calc. HE: {:.3f}s".format(time.time()-t0))
        
        ahh.append(alpha_hh)
    
    ahh = np.array(ahh)       # phase-including pseudo-alpha_HE
    
    
    k0_spectrum = 2 * np.pi / np.array(wavelengths).copy()
    
    dict_pola_HH = dict(
                    sim=sim, r0=r0, r0_MD=r0, 
                    alpha_HH=ahh, 
                    wavelengths=sim.efield.wavelengths, 
                    k0_spectrum=k0_spectrum,
                    )
        
    return dict_pola_HH





def get_eff_polarizability_EE_HH_viaK(sim, method, exact_multipoles=False, verbose=1):
    import time
    if verbose:
        print("Note: field generator will be ignored in the effective polarizability calculation.")
    
    wavelengths = sim.efield.wavelengths
    
    geo = sim.struct.geometry
    r0 = np.average(geo, axis=0)
    
    Dr = geo - r0
    
    ahh = []
    aee = []
    for i_wl, wavelength in enumerate(wavelengths):
        if verbose: 
            t0=time.time()
            print("Wavelength: {}nm".format(wavelength))
        
        ## --- evaluate polarizabilities and normalization terms at "wavelength"
        sim.struct.setWavelength(wavelength)
        n_env = sim.dyads.getEnvironmentIndices(wavelength, sim.struct.geometry)[0]
        k0 = n_env * 2*np.pi / wavelength
        
    ## ----------     calc generalized polarizabilities
        ## phase factor: scalar product k*r
        kr_x = np.multiply(np.array([k0, 0, 0]), Dr)[:,0]
        kr_y = np.multiply(np.array([0, k0, 0]), Dr)[:,1]
        kr_z = np.multiply(np.array([0, 0, k0]), Dr)[:,2]
        eikr_kx = 1*np.exp(1j * kr_x)
        eikr_ky = 1*np.exp(1j * kr_y)
        eikr_kz = 1*np.exp(1j * kr_z)
        
        if exact_multipoles:
            from pyGDM2 import multipole
            K_P_E, K_M_E = multipole.generalized_polarizability(
                    sim, wavelength=wavelength, method=method, verbose=verbose, which_moments=['p', 'm'])
        else:
            K_P_E, K_M_E = get_minipola(sim, wavelength, method, verbose=verbose)
        
    ## ----------     EE
        E0ykx = np.zeros((len(eikr_kx), 3), dtype=np.complex64) 
        E0ykx[:,1] = eikr_kx
        E0zky = np.zeros((len(eikr_kx), 3), dtype=np.complex64) 
        E0zky[:,2] = eikr_ky
        E0xkz = np.zeros((len(eikr_kx), 3), dtype=np.complex64) 
        E0xkz[:,0] = eikr_kz
        _p_ex_kz = np.sum(np.matmul(K_P_E, E0xkz[...,None]), axis=(0,2))
        _p_ey_kx = np.sum(np.matmul(K_P_E, E0ykx[...,None]), axis=(0,2))
        _p_ez_ky = np.sum(np.matmul(K_P_E, E0zky[...,None]), axis=(0,2))
    
    
        ## use same phase terms as for HH 
        alpha_ee = np.array([_p_ex_kz, _p_ey_kx, _p_ez_ky]).T

    ## ---------     HH via EE
        _m_hx_ky = np.sum(np.matmul(K_M_E, E0zky[...,None]), axis=(0,2))
        _m_hy_kz = np.sum(np.matmul(K_M_E, E0xkz[...,None]), axis=(0,2))
        _m_hz_kx = np.sum(np.matmul(K_M_E, E0ykx[...,None]), axis=(0,2))
        
        alpha_hh = -np.array([_m_hx_ky, _m_hy_kz, _m_hz_kx]).T
        
        if verbose: print("calc. HE: {:.3f}s".format(time.time()-t0))
        
        aee.append(alpha_ee)
        ahh.append(alpha_hh)
        
    aee = np.array(aee)       # phase-including pseudo-alpha_HE
    ahh = np.array(ahh)       # phase-including pseudo-alpha_HE
    
    k0_spectrum = 2 * np.pi / np.array(wavelengths).copy()
    
    dict_pola_EE_HH = dict(
                    sim=sim, r0=r0, r0_MD=r0, r0_ED=r0, 
                    alpha_EE=aee, 
                    alpha_HH=ahh, 
                    wavelengths=sim.efield.wavelengths, 
                    k0_spectrum=k0_spectrum,
                     )
        
    return dict_pola_EE_HH






# =============================================================================
# rotate alpha tensor via coordinate transformation
# =============================================================================
def rotate_alpha(alpha_spectra_dict, angle, axis='z'):
    from pyGDM2 import structures
    
    al = angle * np.pi/180
    
    sim0 = alpha_spectra_dict["sim"]
    r0_0 = alpha_spectra_dict["r0"]
    wl = alpha_spectra_dict["wavelengths"]
    a_ee0 = alpha_spectra_dict["alpha_EE"]
    
    
    ## --- rotate structure in simulation
    geo1 = sim0.struct.geometry.copy()
    geo1.T[0] -= r0_0[0]
    geo1.T[0] -= r0_0[1]
    geo1.T[0] -= r0_0[2]
    geo1 = structures.rotate(geo1, angle, axis)  # rotate around "r0"
    r0_1 = np.array([0,0,0])   # new r0: origin
    sim1 = copy.deepcopy(sim0)
    sim1.struct.geometry = geo1
    
    
    ## --- coordinate system transformation for rotation around Carthesian axis
    ## rotation around z axis:
    if axis.lower()=='x':
        _a_rot = np.array([[1, 0, 0],
                           [0, np.cos(al), -np.sin(al)],
                           [0, np.sin(al), np.cos(al)]], dtype=np.complex64)
    elif axis.lower()=='y':
        _a_rot = np.array([[np.cos(al), 0, np.sin(al)],
                           [0, 1, 0], 
                           [-np.sin(al), 0, np.cos(al)]], dtype=np.complex64)
    elif axis.lower()=='z':
        _a_rot = np.array([[np.cos(al), -np.sin(al), 0],
                           [np.sin(al), np.cos(al), 0],
                           [0, 0, 1]], dtype=np.complex64)
    else:
        raise Exception("rotation axis '{}': Unknown axis description.".format(axis))
    
    alpha_rot = np.zeros(a_ee0.shape, dtype=a_ee0.dtype)
    for i_wl, wavelengt in enumerate(wl):
        alpha_rot[i_wl] = np.dot(np.dot(_a_rot, a_ee0[i_wl]), _a_rot.T)
    
    return dict(sim=sim1, r0=r0_1, wavelengths=sim1.efield.wavelengths, alpha_EE=alpha_rot)




# =============================================================================
# extract pseudo-polarizability  (with plane wave phase term)
# =============================================================================
def get_pseudo_polarizability(sim, method='lu', matrix_setup='numba', verbose=1):
    
    import time
    from pyGDM2.core import get_general_propagator
    if verbose:
        print("Note: field generator will be ignored in the effective polarizability calculation.")
    
    wavelengths = sim.efield.wavelengths
    
    geo = sim.struct.geometry
    r0 = np.average(geo, axis=0)
    r0_ED = r0
    r0_MD = r0
    
    Dr = geo - r0
    
    aee = []
    ahe = []
    aee_phase = []
    ahe_phase = []
    for i_wl, wavelength in enumerate(wavelengths):
        if verbose: 
            t0=time.time()
            print("Wavelength: {}nm".format(wavelength))
        
        ## --- evaluate polarizabilities and normalization terms at "wavelength"
        sim.struct.setWavelength(wavelength)
        # sim.dyad.getNormalization(wavelength)
        alpha = sim.dyads.getPolarizabilityTensor(wavelength, sim.struct)
        n_env = sim.dyads.getEnvironmentIndices(wavelength, sim.struct.geometry)[0]
        k0 = n_env * 2*np.pi / wavelength
        
        ## --- General Propagator: Inversion of CAM0
        K = get_general_propagator(sim, wavelength=wavelength, method=method)
        
        ## --- get explicit matrix
        if method.lower() == 'lu':
            import scipy.linalg as la
            warnings.warn("'lu' is not efficient for polarizability extraction. Consider using e.g. 'scipyinv' instead.")
            K = la.lu_solve(K, np.identity(3*len(sim.struct.geometry)))
        
        ## convert to matrix of 3x3 submatrices coupling dp i and j [ K(r_i, r_j, omega) ]
        if method.lower() in ['cuda', 'cupy']:
            K = K.get()
        if verbose:
            print("         inversion: {:.3f}s, ".format(time.time()-t0), end='')
            t0=time.time()
        
        
    ## ----------     calc polarizabilities
        ## phase factor: scalar product k*r
        kr_x = np.multiply(np.array([k0, 0, 0]), Dr)[:,0]
        kr_y = np.multiply(np.array([0, k0, 0]), Dr)[:,1]
        kr_z = np.multiply(np.array([0, 0, k0]), Dr)[:,2]
        eikr_x = np.exp(1j * kr_x)
        eikr_y = np.exp(1j * kr_y)
        eikr_z = np.exp(1j * kr_z)
        
    
    ## ----------     EE
        t0 = time.time()
        
        ## reshape K to be array of 3x3 tensors
        K2 = K.reshape(len(K)//3, 3, -1, 3).swapaxes(1,2).reshape(-1, 3, 3).reshape(len(K)//3,len(K)//3, 3, 3)
    
        # alpha = sim.struct.getPolarizability(wavelength)
        K2_a_ee = np.matmul(alpha, K2)
        alpha_ee0 = np.sum(K2_a_ee, axis=(1,0))
        
        ## pseudo-polarizabilities
        alpha_ee_ph_x = np.sum(np.sum(K2_a_ee, axis=1) * eikr_x[:,None,None], axis=0)
        alpha_ee_ph_y = np.sum(np.sum(K2_a_ee, axis=1) * eikr_y[:,None,None], axis=0)
        alpha_ee_ph_z = np.sum(np.sum(K2_a_ee, axis=1) * eikr_z[:,None,None], axis=0)
        
        alpha_ee_phase = np.array([alpha_ee_ph_x, alpha_ee_ph_y, alpha_ee_ph_z])
        if verbose: print("calc. EE: {:.3f}s, ".format(time.time()-t0), end='')
        
        
    ## ----------     EH
        t0 = time.time()
        K2_a_he = np.cross(Dr[None,:,None], K2_a_ee)   # r cross K
        ik2 = -(1j*k0/2.)
        alpha_he0 =  ik2 * np.sum(K2_a_he, axis=(1,0))  # i*k/2 * sum
        
        ## pseudo-polarizabilities
        alpha_he_ph_x = (ik2 * np.sum(np.sum(K2_a_he, axis=1) * 
                                                   eikr_x[:,None,None], axis=0))
        alpha_he_ph_y = (ik2 * np.sum(np.sum(K2_a_he, axis=1) * 
                                                   eikr_y[:,None,None], axis=0))
        alpha_he_ph_z = (ik2 * np.sum(np.sum(K2_a_he, axis=1) * 
                                                   eikr_z[:,None,None], axis=0))
        
        alpha_he_phase = np.array([alpha_he_ph_x, alpha_he_ph_y, alpha_he_ph_z])
        
        if verbose: print("calc. HE: {:.3f}s".format(time.time()-t0))
        
        
        aee.append(alpha_ee0)
        aee_phase.append(alpha_ee_phase)
        
        ahe.append(alpha_he0)
        ahe_phase.append(alpha_he_phase)
        
    aee = np.array(aee)                   # static alpha_EE
    ahe = np.array(ahe)                   # static alpha_HE
    aee_phase = np.array(aee_phase)       # phase-including pseudo-alpha_EE
    ahe_phase = np.array(ahe_phase)       # phase-including pseudo-alpha_HE
    
    
    k0_spectrum = 2 * np.pi / np.array(wavelengths).copy()
    
    dict_pola = dict(
                    sim=sim, r0=r0, r0_ED=r0_ED, r0_MD=r0_MD, 
                    alpha_EE=aee, alpha_HE=ahe, 
                    wavelengths=sim.efield.wavelengths, 
                    alpha_EE_phase=aee_phase, alpha_HE_phase=ahe_phase,
                    k0_spectrum=k0_spectrum,
                     )
        
    return dict_pola



def get_minipola(sim, wavelength, method='scipyinv', verbose=1):
    """
    based on long-wavelength approximation
    """
    from pyGDM2 import core
    
    ## ----- prep.
    alpha = sim.dyads.getPolarizabilityTensor(wavelength, sim.struct)
    Dr = sim.struct.geometry - np.average(sim.struct.geometry, axis=0)  # --> r - r0
    k0 = 2.0*np.pi / wavelength
    
    if verbose:
        t0 = time.time()
        print("get gen. prop.:", end='')
    
    ## ----- gen. propagator
    K = core.get_general_propagator(sim, wavelength, method=method, calc_H=False)
    if method.lower() in ['cupy', 'cuda']:
        K = K.get()
    if method.lower() == 'lu':
        import scipy.linalg as la
        K = la.lu_solve(K, np.identity(K[0].shape[0], dtype=K[0].dtype))
    
    if verbose:
        t1 = time.time()
        print(" {:.1f}ms.    ED... ".format((t1-t0)*1000), end='')
    
    K2 = K.reshape(len(K)//3, 3, -1, 3).swapaxes(1,2).reshape(-1, 3, 3).reshape(len(K)//3,len(K)//3, 3, 3)
    
    
    ## ----- electric-electric 'mini-pola'
    K_P_E = np.sum(K2, axis=0)
    K_P_E = np.matmul(alpha, K_P_E)
    
    if verbose:
        print("MD...", end='')
    
    ## ----- electric-magnetic 'mini-pola'
    K_he = np.zeros_like(K)
    for i_dp in range(len(K)):
        K_he.T[i_dp] = np.cross(Dr, K.T[i_dp].reshape((len(K)//3,3))).reshape((len(K),))
    K2_he = K_he.reshape(len(K)//3, 3, -1, 3).swapaxes(1,2).reshape(-1, 3, 3).reshape(len(K)//3,len(K)//3, 3, 3)
        
    K_M_E = np.sum(K2_he, axis=0)
    K_M_E = np.matmul( (-1j*k0/2.)*alpha, K_M_E)

    if verbose:
        t2 = time.time()
        print(" {:.1f}ms.".format((t2-t1)*1000))
        
    return K_P_E, K_M_E








def get_dp_T_matrix(sim, r_dp, fieldindex, method='scipyinv', verbose=1):
    """
    r_dp: list of (x,y,z)-coordinates: origins of dipole sources
    
    returns
    -------
    list of 4 (3x3) transfer matrices: 
        Tpp, Tpm, Tmp, Tmm
    """
    from pyGDM2 import tools, fields
    if np.array(r_dp).ndim == 1:
        r_dp = r_dp[None,:]
    
    step = tools.get_step_from_geometry(sim)
    epsilon = step/1000    
    wl = tools.get_field_indices(sim)[fieldindex]['wavelength']
    
    
    fieldkw = []
    for _r_dp in r_dp:
        x0, y0, z0 = _r_dp
        
        fieldkw.append(dict(x0=x0, y0=y0, z0=z0+epsilon, mx=1, my=0, mz=0))
        fieldkw.append(dict(x0=x0, y0=y0, z0=z0+epsilon, mx=0, my=1, mz=0))
        fieldkw.append(dict(x0=x0, y0=y0, z0=z0+epsilon, mx=0, my=0, mz=1))
    
    ef_p = fields.efield(fields.dipole_electric, [wl], fieldkw)
    ef_m = fields.efield(fields.dipole_magnetic, [wl], fieldkw)
    
    
    def T_mat_eval(sim, ef_gen, wl, field_confs, K_P_E, K_M_E):
        env_dict = sim.dyads.getConfigDictG(wl, sim.struct, ef_gen)
        
        Tp, Tm = [], []
        for kws in field_confs:
            ## P, M from generalized polarizability
            E0 = ef_gen.field_generator(sim.struct.geometry, env_dict, wavelength=wl, **kws)
            
            ## estimate self-polarization
            # r_dp = np.array([kws['x0']-epsilon, kws['y0']-epsilon, kws['z0']-epsilon])
            # searchidx = np.where(np.sum(sim.struct.geometry- r_dp, axis=1)==0)[0]
            # if len(searchidx)>0:
            #     E0[searchidx] = E0[searchidx]*0
            
            _p = np.sum(np.matmul(K_P_E, E0[...,None]), axis=(0,2))
            _m = np.sum(np.matmul(K_M_E, E0[...,None]), axis=(0,2))
            
            Tp.append(_p)
            Tm.append(_m)
        
        return np.array(np.split(np.array(Tp), len(Tp)//3)), \
               np.array(np.split(np.array(Tm), len(Tm)//3))
    
    K_P_E, K_M_E = get_minipola(sim, wl, method=method, verbose=verbose)
    Tpp, Tpm = T_mat_eval(sim, ef_p, wl, fieldkw, K_P_E, -K_M_E)
    Tmp, Tmm = T_mat_eval(sim, ef_m, wl, fieldkw, -K_P_E, K_M_E)
    
    return Tpp, Tpm, Tmp, Tmm

def get_dp_T_matrix_viaK(sim, r_dp, fieldindex, method='scipyinv'):
    """
    r_dp: list of (x,y,z)-coordinates: origins of dipole sources
    
    returns
    -------
    list of 4 (3x3) transfer matrices: 
        Tpp, Tpm, Tmp, Tmm
    """
    from pyGDM2 import tools, fields, multipole
    if np.array(r_dp).ndim == 1:
        r_dp = r_dp[None,:]
    
    _sim = sim.copy()
    step = tools.get_step_from_geometry(sim)
    wl = tools.get_field_indices(_sim)[fieldindex]['wavelength']
    
    E0_p = fields.dipole_electric
    E0_m = fields.dipole_magnetic
    
    fieldkw = []
    for _r_dp in r_dp:
        x0, y0, z0 = _r_dp
        epsilon = step/1000
        fieldkw.append(dict(x0=x0, y0=y0, z0=z0+epsilon, mx=1, my=0, mz=0))
        fieldkw.append(dict(x0=x0, y0=y0, z0=z0+epsilon, mx=0, my=1, mz=0))
        fieldkw.append(dict(x0=x0, y0=y0, z0=z0+epsilon, mx=0, my=0, mz=1))
    
    ef_p = fields.efield(E0_p, [wl], fieldkw)
    ef_m = fields.efield(E0_m, [wl], fieldkw)
    
    def T_mat_eval(_sim, ef_gen):
        _sim.efield = ef_gen
        _sim.scatter(method=method)
        Tp, Tm = [], []
        for fidx, fkw in enumerate(tools.get_field_indices(_sim)):
            # _p, _m = linear.multipole_decomp(_sim, fidx)
            # _p, _m, _rp,_rm = decomp_ed_md(_sim, fidx)
            _p, _m = multipole.multipole_decomposition_exact(_sim, fidx, which_moments=['p', 'm'])
            _m = -_m
            Tp.append(_p)
            Tm.append(_m)
        
        return np.array(np.split(np.array(Tp), len(Tp)//3)), \
               np.array(np.split(np.array(Tm), len(Tm)//3))
    
    Tpp, Tpm = T_mat_eval(_sim, ef_p)
    Tmp, Tmm = T_mat_eval(_sim, ef_m)
    
    return Tpp, Tpm, -Tmp, -Tmm

# =============================================================================
# polarizability evaluation functions
# =============================================================================

#%% illuminate and use polarizabilities
# =============================================================================
# illuminate polarizabilites --> dipole moments
# =============================================================================

def illuminate_polarizabilities(dict_alpha_list, field_kwargs):
    """illuminate pseudo-polarizabilities with 'evanescent_planewave' field
    
    
    Parameters
    ----------
    dict_alpha_list : dict
        E/M polarizability description
    
    field_kwargs : dict
        kwargs for `pyGDM2.fields.evanescent_planewave`

    Returns
    -------
    P_M_dict : dict
        dict containing positions and vectors of ED / MD moments

    """
    from pyGDM2 import tools
    from pyGDM2 import fields
    
    field_gen = fields.evanescent_planewave
    
    wavelengths = dict_alpha_list[0]["wavelengths"]
    for dict_a_test in dict_alpha_list:
        if np.sum(np.abs(dict_a_test["wavelengths"] - wavelengths)) != 0:
            raise ValueError("All alpha-dicts must be using the same wavelengths!")
    
    teta_inc = field_kwargs["theta_inc"]
    k0x = np.sin(-teta_inc * np.pi/180)
    k0y = 0.0
    k0z = 1*np.cos(teta_inc * np.pi/180)
    kinc = [k0x, k0y, k0z]
    
    pos_ED = np.array([alpha['r0_ED'] for alpha in dict_alpha_list])
    pos_MD = np.array([alpha['r0_MD'] for alpha in dict_alpha_list])
    
    aee_phase = np.array([alpha["alpha_EE_phase"] for alpha in dict_alpha_list])
    ahe_phase = np.array([alpha["alpha_HE_phase"] for alpha in dict_alpha_list])
    
    E0_ED_spectral = np.zeros([len(pos_ED), len(wavelengths), 3], dtype=np.complex)
    E0_MD_spectral = np.zeros([len(pos_ED), len(wavelengths), 3], dtype=np.complex)
    P_spectral = np.zeros([len(pos_ED), len(wavelengths), 3], dtype=np.complex)
    M_spectral = np.zeros([len(pos_ED), len(wavelengths), 3], dtype=np.complex)
    
    for i_wl, wl in enumerate(wavelengths):
        E0_ED = tools.evaluate_incident_field(field_gen, wl, field_kwargs, pos_ED)[:,3:]
        E0_MD = tools.evaluate_incident_field(field_gen, wl, field_kwargs, pos_MD)[:,3:]
        
        ## !!!!! ATTENTION !!!!! ---  so far only working with z-incidence
        for i_geo in range(len(pos_ED)):
            # =============================================================================
            #  interpolation scheme
            # =============================================================================
            P_xyz = []
            M_xyz = []
            for i, k0_i in enumerate(kinc):
                if i==0:
                    ki = np.array([1,0,0]) * k0_i
                if i==1:
                    ki = np.array([0,1,0]) * k0_i
                if i==2:
                    ki = np.array([0,0,1]) * k0_i
                
                if np.linalg.norm(ki) != 0:                    
                    # -------- electric moment
                    kscaling_E = ((np.sum(ki)/np.linalg.norm(ki)) * np.abs(k0_i/np.linalg.norm(kinc)))**2
                    P_i = np.dot(kscaling_E * aee_phase[i_geo, i_wl, i].T, E0_ED[i_geo])
                    P_xyz.append(P_i)
                    
                    # -------- magnetic moment
                    kscaling_M = (np.sum(ki)/np.linalg.norm(ki)) * np.abs(k0_i/np.linalg.norm(kinc))
                    M_i = np.dot(kscaling_M * ahe_phase[i_geo, i_wl, i].T, E0_ED[i_geo])
                    M_xyz.append(M_i)
            
            # # =============================================================================
            # #         magnetic moment
            # # =============================================================================
            # for i, k0_i in enumerate(kinc):
            #     if i==0:
            #         ki = np.array([1,0,0]) * k0_i
            #     if i==1:
            #         ki = np.array([0,1,0]) * k0_i
            #     if i==2:
            #         ki = np.array([0,0,1]) * k0_i
                
            #     if np.linalg.norm(ki) != 0:
                    
            
            
            P_spectral[i_geo, i_wl] = np.sum(P_xyz, axis=0)
            M_spectral[i_geo, i_wl] = np.sum(M_xyz, axis=0)
            
            E0_ED_spectral[i_geo, i_wl] = E0_ED[i_geo]
            E0_MD_spectral[i_geo, i_wl] = E0_MD[i_geo]
            
            
        #     P_i_kx = np.dot(aee_phase[i_geo,i_wl,0].T, E0_ED[i_geo])
        #     P_i_kz = np.dot(aee_phase[i_geo,i_wl,2].T, E0_ED[i_geo])
            
        #     ## --- for testing: Z-incidence
        #     E0_ED_spectral[i_geo, i_wl] = E0_ED[i_geo]
        #     if field_kwargs['theta_inc'] in [0,180]:
        #         P_spectral[i_geo, i_wl] = P_i_kz #np.sum(P_xyz, axis=0))
        #     elif field_kwargs['theta_inc'] in [90,-90,270]:
        #         P_spectral[i_geo, i_wl] = P_i_kx #np.sum(P_xyz, axis=0))
        #     else:
        #         raise Exception("only x or z incidence supported.")
                
        
        # for i_geo in range(len(pos_MD)):
        #     M_i_kx = np.dot(ahe_phase[i_geo,i_wl,0].T, E0_MD[i_geo])
        #     M_i_kz = np.dot(ahe_phase[i_geo,i_wl,2].T, E0_MD[i_geo])
            
        #     ## --- for testing: Z-incidence
        #     E0_MD_spectral[i_geo, i_wl] = E0_MD[i_geo]
        #     if field_kwargs['theta_inc'] in [0,180]:
        #         M_spectral[i_geo, i_wl] = M_i_kz
        #     elif field_kwargs['theta_inc'] in [90,-90,270]:
        #         M_spectral[i_geo, i_wl] = M_i_kx #np.sum(P_xyz, axis=0))
        #     else:
        #         raise Exception("only x or z incidence supported.")
    
    P_M_dict = dict(
                field_kwargs=field_kwargs,
                wavelengths=wavelengths,
                P_spectral=P_spectral, M_spectral=M_spectral, 
                E0_ED_spectral=E0_ED_spectral,
                E0_MD_spectral=E0_MD_spectral,
                pos_ED=pos_ED, pos_MD=pos_MD
                )
    
    return P_M_dict
    


#%%
# # =============================================================================
# # repropagate interpolated P and M for scattering spectra
# # =============================================================================
# from pyGDM2.propagators import G
# from pyGDM2.propagators import G0_HE
# import numba

# ## numba-accelerated multi-position matrix-vector product
# @numba.njit(parallel=True)
# def _calc_multidipole_EE(dp_pos, r_probe, lamda, eps1, eps2, eps3, spacing, M):
#     for i in numba.prange(len(dp_pos)):    # explicit parallel loop
#         _pos = dp_pos[i]
#         for j in range(len(r_probe)):    # explicit parallel loop
#             _r = r_probe[j]
#             xx, yy, zz, xy, xz, yx, yz, zx, zy = G(_pos, _r, lamda, eps1, eps2, eps3, spacing)
#             ## return list of Greens tensors
#             M[i,j,0,0] = xx
#             M[i,j,1,1] = yy
#             M[i,j,2,2] = zz
#             M[i,j,1,0] = yx
#             M[i,j,2,0] = zx
#             M[i,j,0,1] = xy
#             M[i,j,2,1] = zy
#             M[i,j,0,2] = xz
#             M[i,j,1,2] = yz

# @numba.njit(parallel=True)
# def _calc_multidipole_HE(dp_pos, r_probe, lamda, eps, M):
#     for i in numba.prange(len(dp_pos)):    # explicit parallel loop
#         _pos = dp_pos[i]
#         for j in range(len(r_probe)):    # explicit parallel loop
#             _r = r_probe[j]
#             xx, yy, zz, xy, xz, yx, yz, zx, zy = G0_HE(_pos, _r, lamda, eps)
#             ## return list of Greens tensors
#             M[i,j,0,0] = xx
#             M[i,j,1,1] = yy
#             M[i,j,2,2] = zz
#             M[i,j,1,0] = yx
#             M[i,j,2,0] = zx
#             M[i,j,0,1] = xy
#             M[i,j,2,1] = zy
#             M[i,j,0,2] = xz
#             M[i,j,1,2] = yz




# def dipole_moments_nearfield(P_M_dict, r_probe, eps1=1, eps2=1, eps3=1, spacing=5000):
#     """
#     r_probe : 3 lists
#         1st list: x-values, 2nd list: y-values, 3rd list: z values
#     """
    
#     r_probe = r_probe.T
    
#     field_kwargs = P_M_dict["field_kwargs"]
#     wavelengths = P_M_dict["wavelengths"]
#     pos_ED = P_M_dict["pos_ED"]
#     pos_MD = P_M_dict["pos_MD"]
#     P_spectral = P_M_dict["P_spectral"]
#     M_spectral = P_M_dict["M_spectral"]
    
#     if field_kwargs is not None:
#         from pyGDM2 import tools
#         from pyGDM2 import fields
#         field_gen = fields.evanescent_planewave
    
    
    
#     Escat_ED = np.zeros([len(wavelengths), len(r_probe), 6], dtype=np.complex)
#     Escat_MD = np.zeros([len(wavelengths), len(r_probe), 6], dtype=np.complex)
#     Escat_total = np.zeros([len(wavelengths), len(r_probe), 6], dtype=np.complex)
#     Etot_ED = np.zeros([len(wavelengths), len(r_probe), 6], dtype=np.complex)
#     Etot_MD = np.zeros([len(wavelengths), len(r_probe), 6], dtype=np.complex)
#     Etot_total = np.zeros([len(wavelengths), len(r_probe), 6], dtype=np.complex)
#     for iw, wavelength in enumerate(wavelengths):
        
#         ## incident field
#         if field_kwargs is not None:
#             E0_rprobe = tools.evaluate_incident_field(field_gen, wavelength, field_kwargs, r_probe)[:,3:]
#         else:
#             E0_rprobe = np.zeros(r_probe.shape, dtype=np.complex64)
        
#         ## tensors for ED propagation
#         G_EE_list = np.zeros((len(pos_ED), len(r_probe), 3, 3), dtype=np.complex64)
#         _calc_multidipole_EE(pos_ED, r_probe, wavelength, eps1, eps2, eps2, spacing, 
#                              G_EE_list)
        
#         ## tensors for MD propagation
#         G_HE_list = np.zeros((len(pos_MD), len(r_probe), 3, 3), dtype=np.complex64)
#         _calc_multidipole_HE(pos_MD, r_probe, wavelength, eps2,
#                              G_HE_list)
        
#         ## propagate to every position on integration sphere
#         for i_p_r in numba.prange(G_EE_list.shape[1]):
#             _Es_E = np.zeros(3, dtype=np.complex64)
#             _Es_M = np.zeros(3, dtype=np.complex64)
            
#             ## coherent sum all ED
#             for i_geo_r in range(G_EE_list.shape[0]):
#                 _G = G_EE_list[i_geo_r, i_p_r]
#                 _Es_E += np.dot(_G, P_spectral[i_geo_r, iw])
#             Escat_ED[iw, i_p_r] = np.concatenate([r_probe[i_p_r], _Es_E])
#             Etot_ED[iw, i_p_r] = np.concatenate([r_probe[i_p_r], _Es_E + E0_rprobe[i_p_r]])
            
#             ## coherent sum all MD
#             for i_geo_r in range(G_HE_list.shape[0]):
#                 _G = G_HE_list[i_geo_r, i_p_r]
#                 _Es_M += np.dot(_G, M_spectral[i_geo_r,iw])
#             Escat_MD[iw, i_p_r] = np.concatenate([r_probe[i_p_r], _Es_M])
#             Etot_MD[iw, i_p_r] = np.concatenate([r_probe[i_p_r], _Es_M + E0_rprobe[i_p_r]])
            
#             Escat_total[iw, i_p_r] = np.concatenate([r_probe[i_p_r], _Es_E + _Es_M])
#             # if wavelength==700:
#             #     print(np.abs(_Es_E))
#             #     print(np.abs(_Es_M))
#             #     print(np.abs(_Es_E+_Es_M))
#             #     print()
#             Etot_total[iw, i_p_r] = np.concatenate([r_probe[i_p_r], _Es_E + _Es_M + E0_rprobe[i_p_r]])
    
    
#     return Escat_ED, Escat_MD, Escat_total, \
#            Etot_ED, Etot_MD, Etot_total



# def ED_MD_full_scattering(
#                 P_M_dict, eps1=1, eps2=1, eps3=1, spacing=5000,
#                 r=10000, Nteta=9, Nphi=18, 
#                 tetamin=0, tetamax=np.pi, phimin=0, phimax=2*np.pi
#                 ):
#     """integrated farfield scattering intensity on spherical detector screen

#     """
    
#     ## spherical --> cartesian coordinates
#     tetalist = np.ones((Nteta, Nphi))*np.linspace(tetamin, tetamax, Nteta)[:,None]
#     philist = np.ones((Nteta, Nphi))*np.linspace(phimin, phimax, Nphi, endpoint=False)[None,:]
#     xff = (r * np.sin(tetalist) * np.cos(philist)).flatten()
#     yff = (r * np.sin(tetalist) * np.sin(philist)).flatten()
#     zff = (r * np.cos(tetalist)).flatten()
#     r_probe = np.array([xff, yff, zff]).astype(np.float32)
    
#     ## differentials for integration
#     dteta = (tetamax-tetamin)/float(Nteta-1)
#     dphi = 2.*np.pi/float(Nphi)
    
#     ## evaluate field on sphere
#     Escat_ED, Escat_MD, Escat_total, Etot_ED, Etot_MD, Etot_total = dipole_moments_nearfield(
#                                    P_M_dict, r_probe, eps1, eps2, eps3, spacing)
    
#     ## intensity per position on the spherical "screen"
#     Iscat_ED = np.sum(np.abs(Escat_ED[:,:,3:])**2, axis=2)
#     Iscat_MD = np.sum(np.abs(Escat_MD[:,:,3:])**2, axis=2)
#     Iscat_total = np.sum(np.abs(Escat_total[:,:,3:])**2, axis=2)
    
#     ## integrated intensity per wavelength
#     dOmega = r**2 * np.sin(tetalist.flatten()) * dteta * dphi
#     Iscat_ED = np.sum(Iscat_ED * dOmega, axis=1)
#     Iscat_MD = np.sum(Iscat_MD * dOmega, axis=1)
#     Iscat_total = np.sum(Iscat_total * dOmega, axis=1)
    
#     return Iscat_ED, Iscat_MD, Iscat_total











#%% alpha-struct class
# =============================================================================
# polarizability-based 123-dyads and structure classes (no self-terms by default)
# =============================================================================
from pyGDM2 import propagators
class alpha_dyads_G_123_quasistatic(propagators.DyadsQuasistatic123):
    __name__ = "Quasistatic 3D '1-2-3' Green's tensors"
    
    def __init__(self, polarizability_tensors, wavelengths=None, 
                 n1=None, n2=None, n3=None, spacing=5000, **kwargs):
        if n1 is None or n2 is None:
            raise Exception("`n1` and `n2` must be specified!")
        
        super().__init__(n1=n1, n2=n2, n3=n3, spacing=spacing, **kwargs)
        ## set tabulated tensorial polarizability
        if wavelengths is None:
            if type(polarizability_tensors[0]) is not dict:
                raise Exception("Wavelengths not given. If polar.-tensors are not given as dict, the wavelengths must be defined explicitly.")
            self.wavelengths = polarizability_tensors[0]['wavelengths']
        else:
            self.wavelengths = wavelengths
        
        ## if first element of tensor-list is numpy array, use as is
        if type(polarizability_tensors[0]) == np.ndarray:
            self.polarizability_tensors = polarizability_tensors
        ## if first element of tensor-list is dict, extract "alpha_EE" key
        elif type(polarizability_tensors[0]) == dict:
            self.polarizability_tensors = []
            for alpha_dict in polarizability_tensors:
                self.polarizability_tensors.append(alpha_dict["alpha_EE"])
            self.polarizability_tensors = np.array(self.polarizability_tensors)
            self.polarizability_tensors = np.moveaxis(self.polarizability_tensors, 1, 0)
        else:
            raise Exception("unknown data given as 'polarizability_tensor' list.")
        
        ## test consistency (nr of wavelengths / nr of spectral polarizabilities)
        if len(self.wavelengths) != len(self.polarizability_tensors):
            raise Exception("number of wavelengths doesn't match number of polarizability tensors!")

    def __repr__(self):
        """description about simulation environment defined by set of dyads
        """
        out_str =  ' ------ environment -------'
        out_str += '\n ' + self.__name__
        out_str += '\n- adapted for arbitrary polarizabilities -'
        out_str += '\n '
        out_str += '\n' + '   n3 = {}  <-- top'.format(self.n3_material.__name__)
        out_str += '\n' + '   n2 = {}  <-- structure zone (height "spacing" = {}nm)'.format(
                        self.n2_material.__name__, self.spacing)
        out_str += '\n' + '   n1 = {}  <-- substrate'.format(self.n1_material.__name__)
        return out_str
    
    
    ## --- assuming self-terms zero    
    def getSelfTermEE(self, wavelength, struct):
        eps_env = self.getEnvironmentIndices(wavelength, struct.geometry)
        struct.setWavelength(wavelength)
        
        self_term_tensors_EE = np.zeros([len(eps_env), 3, 3], dtype=self.dtypec)
        
        return self_term_tensors_EE
    
    def getSelfTermHE(self, wavelength, struct):
        return self.self_term_EE(wavelength, struct)
    
    
    ## --- fixed polarizability tensors
    def getPolarizabilityTensor(self, wavelength, struct):
        if self.dtypef is None:
            raise ValueError("Error in structure evaluation: 'dtype' not " +
                             "set yet. Please call 'setDtype' first.")
        
        if type(wavelength) not in [list, tuple, np.ndarray]:
            wavelength = [wavelength]
        wl_idx = np.where(self.wavelengths == wavelength)[0][0]
        
        self.alpha_tensor = self.polarizability_tensors[wl_idx]
        
        return self.alpha_tensor
        
 


class alphaStruct(structures.struct):
    """structure container
    
    Inherits from structure.struc and overrides "getPolarizability"

    """
    def __init__(self, geometry):
        """Initialization"""
        from pyGDM2 import materials
        
        ## dummy config for parent class, will be overridden later
        material = materials.dummy(1)
        normalization = 'cube'
        step = 1
        
        super(self.__class__, self).__init__(
                 step=step, geometry=geometry, material=material, 
                 normalization=normalization, check_geometry_consistency=False)
        


def combine_alphastruct_simulations(sim_list):
    """Combine several simulations containing "alphaStruct" structures
    
    Can be used to artificially *disable* optical interactions between several 
    structures
    
    Parameters
    ----------
    sim_list : list of sim
        list of :class:`.simulation` instances, efield and environment configuration
        must be identical for the different simulations
    
    
    Returns
    -------
    sim : :class:`.simulation`
        new simulation with combined geometry
    
    """
    import copy
    combined_sim = copy.deepcopy(sim_list[0])
    
    combined_geo = []
    combined_pola_tensor = []
    for sim in sim_list:
        if len(sim.efield.kwargs_permutations) != len(combined_sim.efield.kwargs_permutations):
            raise ValueError("Unequal simulation configuration. Same number of incident field configs required!")
        if not np.all(sim.efield.wavelengths == combined_sim.efield.wavelengths):
            raise ValueError("Unequal simulation configuration. Same wavelengths required!")
        if sim.efield.field_generator.__name__ != combined_sim.efield.field_generator.__name__:
            raise ValueError("Unequal simulation configuration. Same field generator!")
        if ( (sim.dyads.n1_material.__name__ != combined_sim.dyads.n1_material.__name__) or 
             (sim.dyads.n2_material.__name__ != combined_sim.dyads.n2_material.__name__) or 
             (sim.dyads.n3_material.__name__ != combined_sim.dyads.n3_material.__name__)):
            raise ValueError("Unequal environment configuration. Same refractive indices n1, n2 and n3 required!")
        if sim.struct.step != combined_sim.struct.step:
            raise ValueError("Unequal stepsize. Same step required!")
        if sim.struct.normalization != combined_sim.struct.normalization:
            raise ValueError("Unequal mesh. Same mesh required!")
        if sim.dyads.spacing != combined_sim.dyads.spacing:
            raise ValueError("Unequal spacing parameter. Same spacing size required!")
        # if sim.dyads.radiative_correction != combined_sim.dyads.radiative_correction:
        #     raise ValueError("Unequal radiative corretion config. Must be the same for all simulations!")
        
        combined_geo.append(copy.deepcopy(sim.struct.geometry))
        combined_pola_tensor.append(copy.deepcopy(sim.dyads.polarizability_tensors))
    
    ## -- combined `struct` instance
    combined_sim.struct = alphaStruct(np.concatenate(combined_geo))
    combined_sim.dyads = alpha_dyads_G_123_quasistatic(
                    polarizability_tensors=np.moveaxis(combined_pola_tensor, 1, 0), 
                    wavelengths=combined_sim.dyads.wavelengths,
                    n1=sim.dyads.n1_material, n2=sim.dyads.n2_material, 
                    n3=sim.dyads.n3_material, spacing=sim.dyads.spacing, 
                    radiative_correction=sim.dyads.radiative_correction
                    )
    
    combined_sim.dyads._legacyStructCompatibility(combined_sim.struct)
    
    ## -- combined pre-calculated E-fields (if available)
    if combined_sim.E is not None:
        for i_E in range(len(combined_sim.E)):
            combined_E = []
            for sim in sim_list:
                combined_E.append(sim.E[i_E][1])
            combined_sim.E[i_E][1] = np.concatenate(combined_E, axis=0)
    
    ## test minimum distances to next neighbor on combined geometry. 
    ## must not be < step
    from scipy.spatial import cKDTree as KDTree
    kdtree = KDTree(combined_sim.struct.geometry)
    mindist = kdtree.query(combined_sim.struct.geometry, k=2, 
                 distance_upper_bound=combined_sim.struct.step*2)[0].T[1]
    if mindist.min() < (0.99 * combined_sim.struct.step / combined_sim.struct.normalization):
        raise ValueError("Too small distance between neighbor meshpoints detected. " +
                         "Minimum allowed distance between cells is one stepsize.")
        
    return combined_sim







#%% repropagate E/M dipoles
def repropagate_ED_MD_nf(r_ed, r_md, ed, md, sim, wavelength,
                r_probe,
                eps1=1, eps2=1, eps3=1, spacing=5000):
    """integrated farfield scattering intensity on spherical detector screen

    """
    from pyGDM2 import linear
    
    
    conf_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
    
    Gp = np.zeros((len(r_ed), len(r_probe), 3, 3), dtype = sim.efield.dtypec)
    sim.dyads.eval_G(r_ed, r_probe, sim.dyads.G_EE, wavelength, conf_dict, Gp)
    Gp = np.moveaxis(Gp, 0,2).reshape((len(r_probe), 3, -1))  # re-arrange for direct matrix-vector multiplication
    
    if len(r_md) > 0:
        Gm = np.zeros((len(r_md), len(r_probe), 3, 3), dtype = sim.efield.dtypec)
        sim.dyads.eval_G(r_md, r_probe, sim.dyads.G_HE, wavelength, conf_dict, Gm)
        Gm = np.moveaxis(Gm, 0,2).reshape((len(r_probe), 3, -1))  # re-arrange for direct matrix-vector multiplication
        Gm = -1*Gm   # magnetic dipole m --> E-field (definition is p-->H)
    
    ## propagate fields 
    Escat_p = np.zeros(shape=(len(r_probe), 3), dtype=sim.efield.dtypec)
    linear._calc_repropagation(ed, Escat_p, Gp)
    if len(r_md) > 0:
        Escat_m = np.zeros(shape=(len(r_probe), 3), dtype=sim.efield.dtypec)
        linear._calc_repropagation(md, Escat_m, Gm)
        Escat = (Escat_p + Escat_m)

    if len(r_md) == 0:
        Escat = Escat_p
    
    # Escat = Escat_m
    
    NF_dp = np.concatenate([r_probe, Escat], axis=-1)
    
    return NF_dp



def repropagate_ED_MD_ff(r_ed, r_md, ed, md, sim, wavelength,
                eps1=1, eps2=1, eps3=1, spacing=5000,
                r=100000, 
                tetamin=0, tetamax=np.pi, Nteta=36, 
                phimin=0, phimax=0, Nphi=1,
                return_integrated_I=False
                ):
    """integrated farfield scattering intensity on spherical detector screen

    """
    from pyGDM2 import linear
    
    ## spherical --> cartesian coordinates
    tetalist = np.ones((Nteta, Nphi))*np.linspace(tetamin, tetamax, Nteta)[:,None]
    philist = np.ones((Nteta, Nphi))*np.linspace(phimin, phimax, Nphi, endpoint=False)[None,:]
    xff = (r * np.sin(tetalist) * np.cos(philist)).flatten()
    yff = (r * np.sin(tetalist) * np.sin(philist)).flatten()
    zff = (r * np.cos(tetalist)).flatten()
    r_probe = np.array([xff, yff, zff]).astype(np.float32).T
    
    ## differentials for integration
    dteta = (tetamax-tetamin)/float(Nteta-1)
    dphi = 2.*np.pi/float(Nphi)
    
    conf_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
    
    Gp = np.zeros((len(r_ed), len(r_probe), 3, 3), dtype = sim.efield.dtypec)
    Gm = np.zeros((len(r_md), len(r_probe), 3, 3), dtype = sim.efield.dtypec)
    
    sim.dyads.eval_G(r_ed, r_probe, sim.dyads.G_EE, wavelength, conf_dict, Gp)
    Gp = np.moveaxis(Gp, 0,2).reshape((len(r_probe), 3, -1))  # re-arrange for direct matrix-vector multiplication
    sim.dyads.eval_G(r_md, r_probe, sim.dyads.G_HE, wavelength, conf_dict, Gm)
    Gm = np.moveaxis(Gm, 0,2).reshape((len(r_probe), 3, -1))  # re-arrange for direct matrix-vector multiplication
    Gm = -1*Gm       # magnetic dipole m --> E-field (definition is p-->H)
    
    
    
    ## propagate fields 
    Escat_p = np.zeros(shape=(len(r_probe), 3), dtype=sim.efield.dtypec)
    Escat_m = np.zeros(shape=(len(r_probe), 3), dtype=sim.efield.dtypec)
    linear._calc_repropagation(ed, Escat_p, Gp)
    linear._calc_repropagation(md, Escat_m, Gm)
    Escat = (Escat_p + Escat_m)
    # Escat = Escat_p
    # Escat = Escat_m
    
    NF_dp = np.concatenate([r_probe, Escat], axis=-1)
    I_s = np.sum(np.abs(Escat)**2, axis=-1)
    
    if not return_integrated_I:
        return tetalist, philist, I_s, NF_dp
    else:
        d_solid_surf = r**2 * np.sin(tetalist) * dteta * dphi
        I_tot = np.sum(I_s * d_solid_surf.flatten())
        return I_tot

