# encoding: utf-8
"""
tools for interfacing deep learning models with pygdm

P. Wiecha, 2021

""" 
import time
import copy

import numpy as np

from pygdmtools import misc
from pyGDM2 import tools
from pyGDM2 import propagators_2D



class field_predictor_pyGDM_interface(object):
    
    def __init__(self, model, step=20, norm_wl=1, norm_pol=1, norm_inc_angle=1, 
                 cpu_inference=True, dyads=None):
        if type(model) == str:
            ## if str given, load model assuming keras model
            print('loading ANN model from file... ', end='')
            from tensorflow.keras.models import load_model
            from tensorflow.keras import backend as K
            K.clear_session()
            self.model = load_model(model, compile=False)
            
            if cpu_inference:
                print('Setup CPU inference... ', end='')
                import tensorflow as tf
                physical_devices = tf.config.list_physical_devices('GPU')
                tf.config.set_visible_devices(physical_devices[1:], 'GPU')
            print('Done.')
        else:
            ## use pre-loaded model
            print('Using pre-loaded ANN.')
            self.model = model
        
        self.step = step
        
        self.norm_wl = norm_wl
        self.norm_pol = norm_pol
        self.norm_inc_angle = norm_inc_angle
        
        self.epsshape = self.model.input_shape[0][1:]
        self.efshape = self.model.output_shape[1:]
        # self.dyads = dyads
        if len(self.epsshape)==3:
        # if type(sim.dyads) == propagators_2D.DyadsQuasistatic2D123:
            self.epsshape = [self.epsshape[0]] + [1] + [self.epsshape[1]] + [self.epsshape[2]]
            self.efshape = [self.efshape[0]] + [1] + [self.efshape[1]] + [self.efshape[2]]
    
    
    def sim_geo_to_voxels(self, sim, wl):
        _voxels, eps, _ef = misc.list_to_threeD(sim, field_index=None, wl=wl)
            
        self.eps = np.zeros(self.epsshape, dtype=np.float32) 
        self.eps[(self.epsshape[0]-eps.shape[0])//2:(self.epsshape[0]-eps.shape[0])//2+eps.shape[0],
                 (self.epsshape[1]-eps.shape[1])//2:(self.epsshape[1]-eps.shape[1])//2+eps.shape[1],
                 0:eps.shape[2],0] = eps.real
        self.eps[(self.epsshape[0]-eps.shape[0])//2:(self.epsshape[0]-eps.shape[0])//2+eps.shape[0],
                 (self.epsshape[1]-eps.shape[1])//2:(self.epsshape[1]-eps.shape[1])//2+eps.shape[1],
                 0:eps.shape[2],1] = eps.imag
        
        if type(sim.dyads) == propagators_2D.DyadsQuasistatic2D123:
            self.eps = np.squeeze(self.eps)
        
        return self.eps
        
        
    def sim_EF_to_voxels(self, sim, fieldindex):
        _voxels, _eps, ef = misc.list_to_threeD(sim, fieldindex)
        # if type(self.dyads) == propagators_2D.DyadsQuasistatic2D123:
        #     eps = np.expand_dims(ef, 1)
            
        self.ef = np.zeros(self.efshape, dtype=np.float32) 
        self.ef[(self.efshape[0]-ef.shape[0])//2:(self.efshape[0]-ef.shape[0])//2+ef.shape[0],
                 (self.efshape[1]-ef.shape[1])//2:(self.efshape[1]-ef.shape[1])//2+ef.shape[1],
                 0:ef.shape[2],[0,2,4]] = ef.real
        self.ef[(self.efshape[0]-ef.shape[0])//2:(self.efshape[0]-ef.shape[0])//2+ef.shape[0],
                 (self.efshape[1]-ef.shape[1])//2:(self.efshape[1]-ef.shape[1])//2+ef.shape[1],
                 0:ef.shape[2],[1,3,5]] = ef.imag
        
        eps = self.sim_geo_to_voxels(sim, wl=sim.E[fieldindex][0]['wavelength'])
        
        if type(sim.dyads) == propagators_2D.DyadsQuasistatic2D123:
            self.ef = np.squeeze(self.ef)
            
        return eps, self.ef

    def get_3d_field_3dmaterials(self, Xval, Y_out, step=1):
        H0 = 1/2.
        
        idx_mat = np.logical_or(Xval[...,0]!=0, Xval[...,1]!=0)
        
        NX = Xval.shape[0]
        NY = Xval.shape[1]
        NZ = Xval.shape[2]
        xl = np.linspace(step, step*(NX), NX)
        yl = np.linspace(step, step*(NY), NY)
        zl = np.linspace(0, step*(NZ-1), NZ)
        xP, yP, zP = np.meshgrid(xl, yl, zl)
    
        Ex = Y_out[...,0] + 1j*Y_out[...,1]
        Ey = Y_out[...,2] + 1j*Y_out[...,3]
        Ez = Y_out[...,4] + 1j*Y_out[...,5]
        
        ## --- reconstruct list of field-vectors
        Es = np.transpose([yP.flatten(), xP.flatten(), zP.flatten(), 
                           Ex.flatten(), Ey.flatten(), Ez.flatten()])
        
        ## --- reconstruct coordinates of structure
        geomat_cmplx = np.sqrt(Xval[...,0] + 1j * Xval[...,1])
        
        material_list = geomat_cmplx.flatten()[idx_mat.flatten()]
        geo = np.real(Es.T[:3].T[idx_mat.flatten()])
        geo.T[2] += step*H0
        Es_int = Es.T[3:].T[idx_mat.flatten()].astype(np.complex64)
    
        return geo, material_list, Es_int
    

    def eval_sim(self, sim, verbose=False):
        pol_strings = {'p':0, 's':1}

        geo_eps_wl = dict()
        geo_eps_in = []
        wavelengths = []
        pola = []
        inc_angle = []
        
        t0 = time.time()
        for wl in sim.efield.wavelengths:
            geo_eps_wl[wl] = self.sim_geo_to_voxels(sim, wl=wl)
        
        for fidx, kw in enumerate(tools.get_field_indices(sim)):
            geo_eps_in.append(geo_eps_wl[kw['wavelength']])
            wavelengths.append(kw['wavelength'] / self.norm_wl)
            pola.append(pol_strings[kw['polar']] / self.norm_pol)
            inc_angle.append(kw['theta_inc'] / self.norm_inc_angle)
            
        t1 = time.time()
        pred_input = {'main_input':np.array(geo_eps_in),
                      'wavelength_input':(np.array(wavelengths))[:,None],
                      'polar_input':np.array(pola)[:,None],
                      'inc_angle_input':np.array(inc_angle)[:,None],
                     }
        ## order: epsilon_input, wavelength_input, polar_input, inc_angle_input
        pred_input = [np.array(geo_eps_in), 
                      np.array(wavelengths)[:,None],
                      np.array(pola)[:,None],
                      np.array(inc_angle)[:,None]
                      ]
        if 'voxel_aux_input' in self.model.input_names:
            pred_input['voxel_aux_input'] = np.zeros(list(np.shape(geo_eps_in)[:-1])+[1])
        predict = self.model.predict(pred_input)
        t2 = time.time()
        if verbose:
            print("ANN-timing - setup: {:.1f}ms, ANN-inference: {:.1f}ms".format(1000*(t1-t0), 1000*(t2-t1)))
        
        sim_ann = copy.deepcopy(sim)
        sim_ann.E = [[]]*len(tools.get_field_indices(sim))
        for fidx, kw in enumerate(tools.get_field_indices(sim)):
            if type(sim.dyads) == propagators_2D.DyadsQuasistatic2D123:
                _geo_eps = np.expand_dims(geo_eps_in[fidx], 1)
                _ef_pred = np.expand_dims(predict[fidx], 1)
            else:
                _geo_eps = geo_eps_in[fidx]
                _ef_pred = predict[fidx]
            _geo, _refidx, E_ANN = self.get_3d_field_3dmaterials(
                                                        _geo_eps, _ef_pred, 
                                                        step=self.step)
            
            sim_ann.E[fidx] = [kw, E_ANN]
        
        return sim_ann
    
    
    
    
    
    
    