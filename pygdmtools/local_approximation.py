# encoding: utf-8
"""
local neigborhood approximation tools for pyGDM

P. Wiecha, 2021

"""
import numpy as np
import matplotlib.pyplot as plt

from pyGDM2 import tools
from pyGDM2 import visu



def local_neighborhood_approximation(
                sim_list, local_approx_lim_X, local_approx_lim_Y, 
                do_testplot=False, projection_testplot='xy', 
                do_scatter_simulations=True, method_sim='lu',
                return_simulation_lists='both', verbose=True):
    """performs a local neighborhood-approximated simulation
    
    nearfield coupling and optical interactions are taken into account only in a
    X/Y field of size `local_approx_lim_X`/`local_approx_lim_Y`.
    

    Parameters
    ----------
    sim_list : list of simulation instances
        list of the individual elements, each element is represented by a pyGDM2 simulation.
        The absolute position of the structure is important!
    
    local_approx_lim_X, local_approx_lim_Y : float, float
        X / Y distance after which no optical interactions are taken into account
    
    do_testplot : bool, optional
        whether or not to plot the current element after each position. 
        The default is False.
        
    projection_testplot : str, optional
        which projection to plot in case `do_testplot`==True. The default is 'xy'.
        
    do_scatter_simulations : bool, optional
        Whether to perform the simulations. You normally want this to be True. 
        The default is True.
        
    method_sim : str, optional
        method for pyGDM simulation. The default is 'lu'.
    
    return_simulation_lists : str, optional
        if "sim", returns only the condensed Born and local approximation simulations.
        if "lists", returns lists of simulations instead of the condensed simulations. 
        if "both", returns simulations and lists of individual simulations:
        The default is 'both'.
        
    verbose : bool, optional
        Print runtime info. The default is True.

    Returns
    -------
    return_simulation_lists == 'sim': 
        returns 2 pyGDM simulations: born-sim, local-sim
    
    return_simulation_lists == 'lists': 
        returns 3 lists of each N pyGDM simulations, where N is the initial 
        number of elements. 
            - list of born simulations
            - list of local-simulations **without** the neighborhood structures
            - list of local-simulations **including** the neighborhood structures
    
    return_simulation_lists == 'both': 
        returns the 2 full-geometry simulations as well as the three lists 
        with separated simulations. In that order.

    """
    pos_list = np.array([_s.struct.geometry.mean(axis=0) for _s in sim_list])
    if do_testplot:
        sim_full = tools.combine_simulations(sim_list)
    
    all_sub_sim_born = []
    all_sub_sim_local_single_rod = []
    all_sub_sim_local = []
    for i_p, pos in enumerate(pos_list):
        ## --- identify structures in proximity
        _p_li_loc = pos_list.copy()
        idx_list = np.logical_and(np.abs(_p_li_loc.T[0] - pos[0]) < local_approx_lim_X,
                                  np.abs(_p_li_loc.T[1] - pos[1]) < local_approx_lim_Y)
        sim_idx_loc = np.arange(len(pos_list))[idx_list]
        if verbose:
            print("struct {}/{}: {} neighbors. ".format(i_p+1, len(pos_list), len(sim_idx_loc)), end='')
        
        
        ## --- isolated simulations (= Born-approximation)
        sim_local_center_born = sim_list[i_p].copy()
        if do_scatter_simulations:
            sim_local_center_born.scatter(verbose=verbose, method=method_sim)
        all_sub_sim_born.append(sim_local_center_born)
        
        
        ## --- local approximation: consider only structures in neighborhood
        _sim_list_local = []
        for i_pos in sim_idx_loc:
            _sim = sim_list[i_pos]
            _sim_list_local.append(_sim.copy())
        
        sim_local = tools.combine_simulations(_sim_list_local)
        if do_scatter_simulations:
            sim_local.scatter(verbose=verbose, method=method_sim)
        all_sub_sim_local.append(sim_local)
        
        ## --- extract center structure from "local" simulation
        sim_local_rod_only, _remaining_sim = tools.split_simulation(sim_local, sim_local_center_born.struct.geometry)
        all_sub_sim_local_single_rod.append(sim_local_rod_only)
        
        
        ## testplot
        if do_testplot:
            plt.subplot(aspect='equal')
            visu.structure(sim_full, scale=3, show=0, absscale=1, projection=projection_testplot)
            visu.structure(sim_local, scale=4, show=0, absscale=1, color='b', projection=projection_testplot)
            visu.structure(sim_local_center_born, scale=3, absscale=1, show=0, color='r', projection=projection_testplot)
            plt.autoscale()
            plt.tight_layout()
            plt.show()
        
        
    if return_simulation_lists in ['sim', 'both']:
        sim_local = tools.combine_simulations(all_sub_sim_local_single_rod)
        sim_born = tools.combine_simulations(all_sub_sim_born)
        
    if return_simulation_lists == 'sim':
        return sim_born, sim_local
    elif return_simulation_lists == 'lists':
        return all_sub_sim_born, all_sub_sim_local_single_rod, all_sub_sim_local
    elif return_simulation_lists == 'both':
        return sim_born, sim_local, all_sub_sim_born, all_sub_sim_local_single_rod, all_sub_sim_local

