# encoding: utf-8
#
#Copyright (C) 2017-2021, P. R. Wiecha
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
python implementations of the mixed electric-magnetic Green's Dyadic Method
"""
from __future__ import print_function
from __future__ import absolute_import

import copy
import warnings
import time
import gc

import numpy as np


from pyGDM2 import propagators
from pyGDM2 import structures
from pyGDM2 import core
from pyGDM2.core import _get_free_GPU_memory, _fieldListToSuperVector, _superVectorToFieldList

from . import polarizabilities as pola
from . import misc

import numba



def sum_of_list(l, n):
    if n == 0:
        return l[n]
    return l[n] + sum_of_list(l,n-1)



#%% alpha-struct class
# =============================================================================
# polarizability-based 123-dyads and structure classes (no self-terms by default)
# =============================================================================
## --- multi-dipole / multi-probe propagator evaluation
def greens_tensor_evaluation(dp_pos, r_probe, G_func, wavelength, conf_dict, M, 
                             selfterm=np.zeros((3,3)).astype(np.complex64), 
                             dist_div_G=0.1):
    raise Exception("Not implemented, please propagate the dipole fields in a custom implementation.")

                
@numba.njit(parallel=True, cache=True)
def t_sbs_EE_123_quasistatic(geo, wavelength, selftermsEE, alphaEE, conf_dict, M):
    eps1 = conf_dict['eps1']
    eps2 = conf_dict['eps2']
    eps3 = conf_dict['eps3']
    spacing = np.float32(conf_dict['spacing'].real)
    
    for i in numba.prange(len(geo)):    # explicit parallel loop
        R2 = geo[i]       # "observer"
        for j in range(len(geo)):
            R1 = geo[j]   # emitter
            aj = alphaEE[j]
            ## --- vacuum dyad
            if i==j:
                ## self term
                st = selftermsEE[j]
                xx, yy, zz = st[0,0], st[1,1], st[2,2]
                xy, xz, yx = st[0,1], st[0,2], st[1,0]
                yz, zx, zy = st[1,2], st[2,0], st[2,1]
            else:
                xx, yy, zz, xy, xz, yx, yz, zx, zy = propagators.G0_EE_123(
                            R1, R2, wavelength, eps1, eps2, eps3, spacing)
            
            ## --- 1-2-3 surface dyad (non retarded NF approximation)
            if eps1!=eps2 or eps2!=eps3:
                xxs,yys,zzs,xys,xzs,yxs,yzs,zxs,zys = propagators.Gs_EE_123(
                                  R1, R2, wavelength, eps1, eps2, eps3, spacing)
                ## combined dyad
                xx, yy, zz, xy, xz, yx, yz, zx, zy = xx+xxs, yy+yys, zz+zzs, \
                                                      xy+xys, xz+xzs, yx+yxs, \
                                                      yz+yzs, zx+zxs, zy+zys
            
            ## return invertible matrix:  delta_ij*1 - G[i,j] * alpha[j]
            M[3*i+0, 3*j+0] = -1*(xx*aj[0,0] + xy*aj[1,0] + xz*aj[2,0])
            M[3*i+1, 3*j+1] = -1*(yx*aj[0,1] + yy*aj[1,1] + yz*aj[2,1])
            M[3*i+2, 3*j+2] = -1*(zx*aj[0,2] + zy*aj[1,2] + zz*aj[2,2])
            M[3*i+0, 3*j+1] = -1*(xx*aj[0,1] + xy*aj[1,1] + xz*aj[2,1])
            M[3*i+0, 3*j+2] = -1*(xx*aj[0,2] + xy*aj[1,2] + xz*aj[2,2])
            M[3*i+1, 3*j+0] = -1*(yx*aj[0,0] + yy*aj[1,0] + yz*aj[2,0])
            M[3*i+1, 3*j+2] = -1*(yx*aj[0,2] + yy*aj[1,2] + yz*aj[2,2])
            M[3*i+2, 3*j+0] = -1*(zx*aj[0,0] + zy*aj[1,0] + zz*aj[2,0])
            M[3*i+2, 3*j+1] = -1*(zx*aj[0,1] + zy*aj[1,1] + zz*aj[2,1])
            if i==j:
                M[3*i+0, 3*j+0] += 1
                M[3*i+1, 3*j+1] += 1
                M[3*i+2, 3*j+2] += 1
                
@numba.njit(parallel=True, cache=True)
def t_sbs_HH_123_quasistatic(geo, wavelength, selftermsHH, alphaHH, conf_dict, M):
    eps1 = conf_dict['eps1']
    eps2 = conf_dict['eps2']
    eps3 = conf_dict['eps3']
    spacing = np.float32(conf_dict['spacing'].real)
    
    for i in numba.prange(len(geo)):    # explicit parallel loop
        R2 = geo[i]       # "observer"
        for j in range(len(geo)):
            R1 = geo[j]   # emitter
            aj = alphaHH[j]
            ## --- vacuum dyad
            if i==j:
                ## self term
                st = selftermsHH[j]
                xx, yy, zz = st[0,0], st[1,1], st[2,2]
                xy, xz, yx = st[0,1], st[0,2], st[1,0]
                yz, zx, zy = st[1,2], st[2,0], st[2,1]
            else:
                xx, yy, zz, xy, xz, yx, yz, zx, zy = propagators.G0_EE_123(
                            R1, R2, wavelength, eps1, eps2, eps3, spacing)
            
            ## --- 1-2-3 surface dyad (non retarded NF approximation)
            if eps1!=eps2 or eps2!=eps3:
                xxs,yys,zzs,xys,xzs,yxs,yzs,zxs,zys = propagators.Gs_EE_123(
                                  R1, R2, wavelength, eps1, eps2, eps3, spacing)
                ## combined dyad
                xx, yy, zz, xy, xz, yx, yz, zx, zy = xx+xxs, yy+yys, zz+zzs, \
                                                      xy+xys, xz+xzs, yx+yxs, \
                                                      yz+yzs, zx+zxs, zy+zys
            
            ## return invertible matrix:  delta_ij*1 - G[i,j] * alpha[j]
            M[3*i+0, 3*j+0] = -1*(xx*aj[0,0] + xy*aj[1,0] + xz*aj[2,0])
            M[3*i+1, 3*j+1] = -1*(yx*aj[0,1] + yy*aj[1,1] + yz*aj[2,1])
            M[3*i+2, 3*j+2] = -1*(zx*aj[0,2] + zy*aj[1,2] + zz*aj[2,2])
            M[3*i+0, 3*j+1] = -1*(xx*aj[0,1] + xy*aj[1,1] + xz*aj[2,1])
            M[3*i+0, 3*j+2] = -1*(xx*aj[0,2] + xy*aj[1,2] + xz*aj[2,2])
            M[3*i+1, 3*j+0] = -1*(yx*aj[0,0] + yy*aj[1,0] + yz*aj[2,0])
            M[3*i+1, 3*j+2] = -1*(yx*aj[0,2] + yy*aj[1,2] + yz*aj[2,2])
            M[3*i+2, 3*j+0] = -1*(zx*aj[0,0] + zy*aj[1,0] + zz*aj[2,0])
            M[3*i+2, 3*j+1] = -1*(zx*aj[0,1] + zy*aj[1,1] + zz*aj[2,1])
            if i==j:
                M[3*i+0, 3*j+0] += 1
                M[3*i+1, 3*j+1] += 1
                M[3*i+2, 3*j+2] += 1


@numba.njit(parallel=True, cache=True)
# def t_sbs_HE_123_quasistatic(geo, wavelength, eps1, eps2, eps3, spacing, selfterms, alpha, M):
def t_sbs_HE_123_quasistatic(geo, wavelength, selftermsHE, alphaEE, conf_dict, M):
    eps2 = conf_dict['eps2']
    
    for i in numba.prange(len(geo)):    # explicit parallel loop
        R2 = geo[i]        # "observer"
        for j in range(len(geo)):
            R1 = geo[j]    # emitter
            aj = alphaEE[j]
            ## --- vacuum dyad
            if i==j:
                ## self term
                st = selftermsHE[j]
                xx, yy, zz = st[0,0], st[1,1], st[2,2]
                xy, xz, yx = st[0,1], st[0,2], st[1,0]
                yz, zx, zy = st[1,2], st[2,0], st[2,1]
            else:
                ## we need G^HE: H-field due to e-dipole
                xx, yy, zz, xy, xz, yx, yz, zx, zy = propagators._G0_HE(R1, R2, wavelength, eps2)
            
            ## return: G[i,j] * alpha[j]
            ## --- magnetic-electric part
            M[3*i+0, 3*j+0] = -1*(xx*aj[0,0] + xy*aj[1,0] + xz*aj[2,0])
            M[3*i+1, 3*j+1] = -1*(yx*aj[0,1] + yy*aj[1,1] + yz*aj[2,1])
            M[3*i+2, 3*j+2] = -1*(zx*aj[0,2] + zy*aj[1,2] + zz*aj[2,2])
            M[3*i+0, 3*j+1] = -1*(xx*aj[0,1] + xy*aj[1,1] + xz*aj[2,1])
            M[3*i+0, 3*j+2] = -1*(xx*aj[0,2] + xy*aj[1,2] + xz*aj[2,2])
            M[3*i+1, 3*j+0] = -1*(yx*aj[0,0] + yy*aj[1,0] + yz*aj[2,0])
            M[3*i+1, 3*j+2] = -1*(yx*aj[0,2] + yy*aj[1,2] + yz*aj[2,2])
            M[3*i+2, 3*j+0] = -1*(zx*aj[0,0] + zy*aj[1,0] + zz*aj[2,0])
            M[3*i+2, 3*j+1] = -1*(zx*aj[0,1] + zy*aj[1,1] + zz*aj[2,1])



@numba.njit(parallel=True, cache=True)
def t_sbs_EH_123_quasistatic(geo, wavelength, selftermsEH, alphaHH, conf_dict, M):
    eps2 = conf_dict['eps2']
    
    for i in numba.prange(len(geo)):    # explicit parallel loop
        R2 = geo[i]        # "observer"
        for j in range(len(geo)):
            R1 = geo[j]    # emitter
            aj = alphaHH[j]
            ## --- vacuum dyad
            if i==j:
                ## self term
                st = selftermsEH[j]
                xx, yy, zz = st[0,0], st[1,1], st[2,2]
                xy, xz, yx = st[0,1], st[0,2], st[1,0]
                yz, zx, zy = st[1,2], st[2,0], st[2,1]
            else:
                ## we need G^HE: H-field due to e-dipole
                ## G_EH_ij = G_HE_ji (HE --> EH :  R1,R2 --> R2,R1)
                xx, yy, zz, xy, xz, yx, yz, zx, zy = propagators._G0_HE(R2, R1, wavelength, eps2)
            
            ## return: G[i,j] * alpha[j]
            ## --- magnetic-electric part
            M[3*i+0, 3*j+0] = -1*(xx*aj[0,0] + xy*aj[1,0] + xz*aj[2,0])
            M[3*i+1, 3*j+1] = -1*(yx*aj[0,1] + yy*aj[1,1] + yz*aj[2,1])
            M[3*i+2, 3*j+2] = -1*(zx*aj[0,2] + zy*aj[1,2] + zz*aj[2,2])
            M[3*i+0, 3*j+1] = -1*(xx*aj[0,1] + xy*aj[1,1] + xz*aj[2,1])
            M[3*i+0, 3*j+2] = -1*(xx*aj[0,2] + xy*aj[1,2] + xz*aj[2,2])
            M[3*i+1, 3*j+0] = -1*(yx*aj[0,0] + yy*aj[1,0] + yz*aj[2,0])
            M[3*i+1, 3*j+2] = -1*(yx*aj[0,2] + yy*aj[1,2] + yz*aj[2,2])
            M[3*i+2, 3*j+0] = -1*(zx*aj[0,0] + zy*aj[1,0] + zz*aj[2,0])
            M[3*i+2, 3*j+1] = -1*(zx*aj[0,1] + zy*aj[1,1] + zz*aj[2,1])




class alpha_dyads_G_123_quasistatic(propagators.DyadsQuasistatic123):
    __name__ = "Quasistatic 3D '1-2-3' Green's tensors"
    
    def __init__(self, alpha_EE, alpha_HH=None, 
                 selfterms_EE=None, selfterms_HH=None,
                 wavelengths=None, n1=None, n2=None, n3=None, spacing=10000, 
                 **kwargs):
        """
        The complex polarizabilities (alpha_xx) and selfterms need to be lists 
        of pola-dicts or numpy ndarrays. 
        
        If they are np ndarrays, they mneed following dimensions: 
        [N wavelength, M scatterer, 3, 3].
        In latter case, the wavelengths need to be defined in seperate argument.
        
        The position of each scatterer is defined in the separate 
        `pyGDM2.structures.struct` object.
        
        """
        if n1 is None or n2 is None:
            raise Exception("`n1` and `n2` must be specified!")
        
        super().__init__(n1=n1, n2=n2, n3=n3, spacing=spacing, **kwargs)
        ## set tabulated tensorial polarizability
        if wavelengths is None:
            if type(alpha_EE[0]) is not dict:
                raise Exception("Wavelengths not given. If polar.-tensors are not given as dict, the wavelengths must be defined explicitly.")
            self.wavelengths = alpha_EE[0]['wavelengths']
        else:
            self.wavelengths = wavelengths
        
        ## config coupled system functions
        self.tsbs_EE = t_sbs_EE_123_quasistatic
        self.tsbs_HH = t_sbs_HH_123_quasistatic
        self.tsbs_EH = t_sbs_EH_123_quasistatic
        self.tsbs_HE = t_sbs_HE_123_quasistatic
        
        ## ------------- EE polarizability
        ## if first element of tensor-list is numpy array, use as is
        if type(alpha_EE[0]) == np.ndarray:
            self.polarizability_EE = alpha_EE
        ## if first element of tensor-list is dict, extract "alpha_EE" key
        elif type(alpha_EE[0]) == dict:
            self.polarizability_EE = []
            for alpha_dict in alpha_EE:
                self.polarizability_EE.append(alpha_dict["alpha_EE"])
            self.polarizability_EE = np.array(self.polarizability_EE, dtype=np.complex64)
            self.polarizability_EE = np.moveaxis(self.polarizability_EE, 1, 0)
            if 'alpha_HH' in alpha_EE[0].keys() and alpha_HH is None:
                alpha_HH = alpha_EE
        else:
            raise Exception("unknown data given as 'alpha_EE' list.")
        
        
        
        ## ------------- HH polarizability
        ## not given: zero
        if alpha_HH is None:
            self.polarizability_HH = np.zeros(shape=self.polarizability_EE.shape, dtype=np.complex64)
        else:
            ## if first element of tensor-list is numpy array, use as is
            if type(alpha_HH[0]) == np.ndarray:
                self.polarizability_HH = alpha_HH
            ## if first element of tensor-list is dict, extract "alpha_EE" key
            elif type(alpha_HH[0]) == dict:
                self.polarizability_HH = []
                for alpha_dict in alpha_HH:
                    self.polarizability_HH.append(alpha_dict["alpha_HH"])
                self.polarizability_HH = np.array(self.polarizability_HH, dtype=np.complex64)
                self.polarizability_HH = np.moveaxis(self.polarizability_HH, 1, 0)
            else:
                raise Exception("unknown data given as 'alpha_HH' list.")
        
        
        ## ------------- self-terms
        if selfterms_EE is not None:
            self.self_term_tensors_EE = selfterms_EE
        else:
            self.self_term_tensors_EE = np.zeros(shape=self.polarizability_EE.shape, dtype=np.complex64)
            
        if selfterms_HH is not None:
            self.self_term_tensors_HH = selfterms_HH
        else:
            self.self_term_tensors_HH = np.zeros(shape=self.polarizability_HH.shape, dtype=np.complex64)
        
        
        ## test consistency (nr of wavelengths / nr of spectral polarizabilities)
        if len(self.wavelengths) != len(self.polarizability_EE):
            raise Exception("number of wavelengths doesn't match number of EE polarizability tensors!")
        if len(self.wavelengths) != len(self.polarizability_HH):
            raise Exception("number of wavelengths doesn't match number of HH polarizability tensors!")


    def exceptionHandling(self, struct, efield):
        """Exception handling / consistency check for the set of tensors
        
        check if structure and incident field generator are compatible

        Parameters
        ----------
        struct : :class:`.structures.struct`
            instance of structure class
        efield : :class:`.fields.efield`
            instance of incident field class

        Returns
        -------
        bool : True if struct and field are compatible, False if they don't fit the tensors

        """
        if len(struct.geometry) > 0:
            z_min = struct.geometry.T[2].min()
            z_max = struct.geometry.T[2].max()
            ## if interface 1/2 exists, check if entire structure is below or above
            if self.n1_material.__name__ != self.n2_material.__name__:
                if z_min<0 and z_max>0:
                    warnings.warn("Structure in-between substrate and middle-layer. " +
                                  "This is not supported and will most likely falsify the simulation.")
            
            ## if interface 2/3 exists, check if entire structure is below or above
            if self.n2_material.__name__ != self.n3_material.__name__:
                if z_min<self.spacing and z_max>self.spacing:
                    warnings.warn("Structure in-between middle and top cladding layer. " +
                                  "This is not supported and will most likely falsify the simulation.")
        
        ## check if correct number of meshpoints / polarizabilities is defined
        if len(struct.geometry) != self.polarizability_EE.shape[1] or \
           len(struct.geometry) != self.polarizability_HH.shape[1]:
               raise Exception('for every meshpoint, exactly one polarizability must be defined! ' +
                               'But: {} meshpoints <--> {} polarizabilities'.format(
                                   len(struct.geometry), self.polarizability_EE.shape[1]))
        
        return True


    def __repr__(self, verbose=True):
        """description about simulation environment defined by set of dyads
        """
        out_str =  ' ------ environment -------'
        out_str += '\n ' + self.__name__
        out_str += '\n- adapted for arbitrary EE +HH polarizabilities per cell -'
        out_str += '\n '
        out_str += '\n' + '   n3 = {}  <-- top'.format(self.n3_material.__name__)
        out_str += '\n' + '   n2 = {}  <-- structure zone (height "spacing" = {}nm)'.format(
                        self.n2_material.__name__, self.spacing)
        out_str += '\n' + '   n1 = {}  <-- substrate'.format(self.n1_material.__name__)
        return out_str
    
    
    def getSelfTermEE(self, wavelength, struct):
        if self.dtypef is None:
            raise ValueError("Error in structure evaluation: 'dtype' not " +
                             "set yet. Please call 'setDtype' first.")
        
        if type(wavelength) not in [list, tuple, np.ndarray]:
            wavelength = [wavelength]
        wl_idx = np.where(self.wavelengths == wavelength)[0][0]
        
        st_tensor = self.self_term_tensors_EE[wl_idx]
        
        # # radiative correction
        # k0 = 2*np.pi / wavelength
        # radnorm = 1j * 2.0 * (k0**3)/3.0 #* np.ones(len(st_tensor))
        # st_tensor[0,0] += radnorm
        # st_tensor[1,1] += radnorm
        # st_tensor[2,2] += radnorm
        
        
        return st_tensor
    
    def getSelfTermHH(self, wavelength, struct):
        if self.dtypef is None:
            raise ValueError("Error in structure evaluation: 'dtype' not " +
                             "set yet. Please call 'setDtype' first.")
        
        if type(wavelength) not in [list, tuple, np.ndarray]:
            wavelength = [wavelength]
        wl_idx = np.where(self.wavelengths == wavelength)[0][0]
        
        st_tensor = self.self_term_tensors_HH[wl_idx]
        
        return st_tensor
    
    ## --- assuming mixed self-terms zero    
    def getSelfTermHE(self, wavelength, struct):
        return np.zeros(shape=self.polarizability_EE.shape[1:], dtype=np.complex64)
    def getSelfTermEH(self, wavelength, struct):
        return np.zeros(shape=self.polarizability_EE.shape[1:], dtype=np.complex64)
    
    
    ## --- fixed polarizability tensors
    def getPolarizabilityTensorEE(self, wavelength, struct):
        if self.dtypef is None:
            raise ValueError("Error in structure evaluation: 'dtype' not " +
                             "set yet. Please call 'setDtype' first.")
        
        if type(wavelength) not in [list, tuple, np.ndarray]:
            wavelength = [wavelength]
        wl_idx = np.where(self.wavelengths == wavelength)[0][0]
        
        alpha_tensor = self.polarizability_EE[wl_idx]
        
        return alpha_tensor
    
    def getPolarizabilityTensorHH(self, wavelength, struct):
        if self.dtypef is None:
            raise ValueError("Error in structure evaluation: 'dtype' not " +
                             "set yet. Please call 'setDtype' first.")
        
        if type(wavelength) not in [list, tuple, np.ndarray]:
            wavelength = [wavelength]
        wl_idx = np.where(self.wavelengths == wavelength)[0][0]
        
        alpha_tensor = self.polarizability_HH[wl_idx]
        
        return alpha_tensor
        
 


class alphaStruct(structures.struct):
    """structure container
    
    Inherits from structure.struct
    material, normalization and step are dummy values

    """
    def __init__(self, geometry):
        """Initialization"""
        from pyGDM2 import materials
        
        ## dummy config for parent class, will be overridden later
        material = materials.dummy(1)
        normalization = 'cube'
        step = 1.0
        
        super(self.__class__, self).__init__(
                 step=step, geometry=geometry, material=material, 
                 normalization=normalization, check_geometry_consistency=False)
    
    
    def __repr__(self, verbose=False):
        out_str = ''
        out_str += ' ------ nano-object -------'
        out_str += '\n' + '   object defined by individual polarizabilities. '
        out_str += '\n' + '   mesh type:            {}'.format(self.meshtype)
        out_str += '\n' + '   nominal stepsize:     {}nm'.format(self.step)
        out_str += '\n' + '   nr. of meshpoints:    {}'.format(self.n_dipoles)
        if verbose:
            out_str += '\n' + '     X-extension    :    {:.1f} - {:.1f} (nm)'.format(self.geometry.T[0].min(), self.geometry.T[0].max() )
            out_str += '\n' + '     Y-extension    :    {:.1f} - {:.1f} (nm)'.format(self.geometry.T[1].min(), self.geometry.T[1].max() )
            out_str += '\n' + '     Z-extension    :    {:.1f} - {:.1f} (nm)'.format(self.geometry.T[2].min(), self.geometry.T[2].max() )
        return out_str




# =============================================================================
# get coupled ED/MD systems of equations
# =============================================================================
def get_SBS_EE(sim, wavelength):
    ##  --- simulation config
    geo = sim.struct.geometry
    
    ## --- material permittivity related config
    self_term_tensors = sim.dyads.getSelfTermEE(wavelength, sim.struct)
    alpha_tensors = sim.dyads.getPolarizabilityTensorEE(wavelength, sim.struct)

    ## --- run numba routine for EE coupling
    M_EE = np.zeros((len(geo)*3,len(geo)*3), dtype=sim.dtypec)
    conf_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
    # sim.dyads.tsbs_EE(geo, wavelength, self_term_tensors, alpha_tensors, 
    propagators.t_sbs_EE_123_quasistatic(geo, wavelength, self_term_tensors, alpha_tensors, 
                      conf_dict, M_EE)
    
    return M_EE


def get_SBS_HH(sim, wavelength):
    ##  --- simulation config
    geo = sim.struct.geometry
    
    ## --- material permittivity related config
    self_term_tensors = sim.dyads.getSelfTermHH(wavelength, sim.struct)
    alpha_tensors = sim.dyads.getPolarizabilityTensorHH(wavelength, sim.struct)

    ## --- run numba routine for HH coupling
    M_HH = np.zeros((len(geo)*3,len(geo)*3), dtype=sim.dtypec)
    conf_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
    sim.dyads.tsbs_HH(geo, wavelength, self_term_tensors, alpha_tensors, 
                      conf_dict, M_HH)
    
    return M_HH


def get_SBS_HE(sim, wavelength):
    ##  --- simulation config
    geo = sim.struct.geometry
    
    ## --- material permittivity related config
    self_term_tensors = sim.dyads.getSelfTermHE(wavelength, sim.struct)
    alpha_tensors = sim.dyads.getPolarizabilityTensorEE(wavelength, sim.struct)
    
    ## --- run numba routine for EH coupling
    M_HE = np.zeros((len(geo)*3,len(geo)*3), dtype=sim.dtypec)
    conf_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
    sim.dyads.tsbs_HE(geo, wavelength, self_term_tensors, alpha_tensors, 
                      conf_dict, M_HE)
    
    return M_HE


def get_SBS_EH(sim, wavelength):
    ##  --- simulation config
    geo = sim.struct.geometry
    
    ## --- material permittivity related config
    self_term_tensors = sim.dyads.getSelfTermEH(wavelength, sim.struct)
    alpha_tensors = sim.dyads.getPolarizabilityTensorHH(wavelength, sim.struct)
    
    ## --- run numba routine for EH coupling
    M_EH = np.zeros((len(geo)*3,len(geo)*3), dtype=sim.dtypec)
    conf_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
    sim.dyads.tsbs_EH(geo, wavelength, self_term_tensors, alpha_tensors, 
                      conf_dict, M_EH)
    
    return M_EH


def get_general_propagator(sim=None, wavelength=None, method='lu', verbose=False,
                           coupling_terms=['ee', 'hh', 'eh', 'he']):
    """invert electric+magnetic dipole coupling matrix
    
    Parameters
    ----------
    sim : :class:`.simulation`
        simulation description
    
    wavelength: float
        Wavelength at which to calculate susceptibility matrix (in nm)
    
    method : string, default: "lu"
        inversion method. One of ["lu", "scipyinv", "cupy"]
         - "lu" LU-decomposition (`scipy.linalg.lu_factor`)
         - "scipyinv" scipy default inversion (`scipy.linalg.inv`)
         - "cupy" uses CUDA GPU via `cupy`
    
    verbose : bool, default False
        Print timing info to stdout
        
    
    Returns
    -------
      - K: Generalized Propagator
    
    
    Notes
    -----
    For details on the concept of the generalized propagator, see e.g.:
    Martin, O. J. F. & Girard, C. & Dereux, A. **Generalized Field Propagator 
    for Electromagnetic Scattering and Light Confinement.**
    Phys. Rev. Lett. 74, 526–529 (1995).
    
    For the Electric-magnetic mixed field calculation, see e.g.:
    Schröter, U. **Modelling of magnetic effects in near-field optics.** 
    Eur. Phys. J. B 33, 297–310 (2003).
    """
# =============================================================================
#     Exception handling
# =============================================================================
    if method.lower() == "cuda":
        method = "cupy"
        
    if method.lower() == "cupy":
        import cupy
        if int(cupy.__version__.split('.')[0])<7:
            raise ValueError("`cupy` version 7 or higher required. Found cupy " +
                             "version {}. Please upgrade cupy.".format(cupy.__version__))
    
    if method.lower() not in ["lu", "scipyinv", "cupy"]:
        raise ValueError('Invalid inversion method. Must be one of ["lu", ' +
                         '"scipyinv", "cupy"].')
    
    
# =============================================================================
#     setup EE coupling matrix
# =============================================================================
    coupling_terms=[c_t.lower() for c_t in coupling_terms]
    
    t0 = time.time()
    ## --- construct matrix
    geo = sim.struct.geometry
    M_ii = np.identity(len(geo)*3, dtype=sim.dtypec)
    M_EE = M_ii.copy()
    M_HH = M_ii.copy()
    M_ij = np.zeros((len(geo)*3,len(geo)*3), dtype=sim.dtypec)
    M_EH = M_ij.copy()
    M_HE = M_ij.copy()
    if 'ee' in coupling_terms:
        M_EE = get_SBS_EE(sim, wavelength=wavelength)
    if 'hh' in coupling_terms:
        M_HH = get_SBS_HH(sim, wavelength=wavelength)
    if 'eh' in coupling_terms:
        M_EH = get_SBS_EH(sim, wavelength=wavelength)
    if 'he' in coupling_terms:
        M_HE = get_SBS_HE(sim, wavelength=wavelength)
    
    M = np.concatenate([np.concatenate([M_EE, M_EH], axis=1), 
                        np.concatenate([M_HE, M_HH], axis=1)])
    
    if verbose:
        print('timing for wl={:.2f}nm - setup: EE {:.1f}ms, '.format(
                            wavelength, 1000.*(time.time()-t0)), end='')
    t0b = time.time()
    
# =============================================================================
#    inversion of EE part (via chosen method)
# =============================================================================
    ## --- CUDA based inversion on GPU via `cupy`
    if method.lower() == "cupy":
        import cupy as cp
        import cupy.linalg
        ## free no longer used GPU RAM
        mempool = cp.get_default_memory_pool()
        mempool.free_all_blocks()
        
        ## check available memory:
        free_mem = _get_free_GPU_memory()
        if free_mem == -1:
            warnings.warn("Failed checking available GPU memory. Install `nvidia-ml-py3` (e.g. via pip) if you want the GPU-RAM test to work.")
        else:
            req_mem = 4 * M.nbytes/1024**2    # 'inv' requires memorey of 4*array
            if req_mem > free_mem:
                warnings.warn("Required memory exceeds GPU RAM. Falling back to scipy's 'scipyinv'. You could also try 'LU' instead, which is often more efficient.")
                method = 'scipyinv'
            else:
                ## move array to cuda device, cuda inversion
                Agpu = cp.array(np.ascontiguousarray(M))
                Ainv_gpu = cupy.linalg.inv(Agpu)
                K = Ainv_gpu
                
                mempool.free_all_blocks()    # free no longer used GPU RAM
    
    
    ## --- CPU inversion methods
    if method.lower() in ["scipyinv", "lu"]:
        if method.lower() in ["scipyinv"]:
            import scipy.linalg as la
            K = la.inv(M, overwrite_a=True)
        elif method.lower() in ["lu"]:
            import scipy.linalg as la
            K = la.lu_factor(M, overwrite_a=True)
        del M; gc.collect()
    
    
    t1 = time.time()
# =============================================================================
# Done
# =============================================================================
    if verbose: 
        print('inv.: {:.1f}ms, '.format(1000.*(t1-t0b)), end='')
    
    return K


#==============================================================================
# scattered fields
#==============================================================================
def scatter(sim, method='lu', verbose=True, callback=None, 
            coupling_terms=['ee', 'hh', 'eh', 'he'], **kwargs):
    """Perform a linear scattering GDM simulation with elecrtic and magnetic dipoles
    
    Calculate the electric field distribution inside a nano-structure.
    Optionally calculate also the internal magnetic field.
    
    Parameters
    ----------
    
    coupling terms: list of str, default: ['ee', 'hh', 'eh', 'he']
        which electric-magnetic coupling to include in simulation
    
    see doc of `pyGDM2.core.scatter`
        
    """
# =============================================================================
#     Exception handling
# =============================================================================
    if method.lower() == "cuda":
        method = "cupy"
        
    if method.lower() not in ["scipyinv", "lu", "cupy"]:
        raise ValueError('Error: Unknown solving method. Must be one of' +
                         ' ["lu", "scipyinv", "cupy"].')
    
    if len(coupling_terms) < 4:
        warnings.warn('Not all coupling terms taken into account! Please verify if this is deliberate?')
    
# =============================================================================
#     iterate wavelengths
# =============================================================================
    field_generator = sim.efield.field_generator
    wavelengths = sim.efield.wavelengths
    
    scattered_fields_E = []
    scattered_fields_H = []
    for i_wl, wavelength in enumerate(wavelengths):
        
# =============================================================================
#     get generalized propagator
# =============================================================================
        t0 = time.time()
        K = get_general_propagator(sim=sim, method=method, wavelength=wavelength,
                                   coupling_terms=coupling_terms, verbose=verbose)
        t1 = time.time()
        
#==============================================================================
#    each wl: Incident field evaluation
#==============================================================================
        ## --- At fixed wavelength: Use generalized propagator on all incident field parameters
        def generalized_propagator_operation(field_kwargs, K, sim, wavelength):
            env_dict = sim.dyads.getConfigDictG(wavelength, sim.struct, sim.efield)
            
            ## --- get E0
            E0 = field_generator(sim.struct.geometry, env_dict, wavelength, **field_kwargs)
            E0_supervec = _fieldListToSuperVector(E0)
            
            ## --- get H0
            field_kwargs_H = copy.deepcopy(field_kwargs)
            field_kwargs_H["returnField"] = "H"
            H0 = field_generator(sim.struct.geometry, env_dict, wavelength, **field_kwargs_H)
            H0_supervec = _fieldListToSuperVector(H0)
            F0_supervec = np.concatenate([E0_supervec, H0_supervec])
            
            ## --- generalized propagator times incident field:
            if method.lower() == 'lu':
                import scipy.linalg as la
                F = la.lu_solve(K, F0_supervec)
            elif method.lower() == 'cupy' and type(K) != np.ndarray:
                ## --- cupy GPU matrix vector multiplication
                import cupy as cp
                F0_supervec_gpu = cp.array(np.ascontiguousarray(F0_supervec))  # move vec to device memory
                F_gpu = cp.dot(K, F0_supervec_gpu)
                F = F_gpu.get()
            else:   # scipyinv
                F = np.dot(K, F0_supervec)
            
            E = _superVectorToFieldList(F[:3*len(sim.struct.geometry)])
            H = _superVectorToFieldList(F[3*len(sim.struct.geometry):])
            
            
            kwargs_final = copy.deepcopy(field_kwargs)
            kwargs_final["wavelength"] = wavelength
            
            ## --- optional: H
            return dict(kw=kwargs_final, E=E, H=H)


        
        ## --- loop over incident field configurations
        for field_kwargs in sim.efield.kwargs_permutations:
            scat_results = generalized_propagator_operation(field_kwargs, K, sim, wavelength)
            scattered_fields_E.append([scat_results['kw'], scat_results['E']]) 
            scattered_fields_H.append([scat_results['kw'], scat_results['H']])
            sim.E = scattered_fields_E
            sim.H = scattered_fields_H
                
        if verbose: print("repropa.: {:.1f}ms ({} field configs), tot: {:.1f}ms".format(
                                        1000.*(time.time()-t1), 
                                        len(sim.efield.kwargs_permutations), 
                                        1000.*(time.time()-t0)))
# =============================================================================
#       if applicable: call callback
# =============================================================================
        if callback is not None:
            cb_continue = callback(dict(
                i_wl=i_wl, wavelength=wavelength, sim=sim,
                t_inverse=1000.*(t1-t0), t_repropa=1000.*(time.time()-t1))
                                  )
            if not cb_continue:
                return -1
        
        ## free RAM for next iteration
        del K
        gc.collect()
    
    return 1



# =============================================================================
# other helper functions
# =============================================================================
def combine_alphastruct_simulations(sim_list):
    """Combine several simulations containing "alphaStruct" structures
    
    Can be used to artificially *disable* optical interactions between several 
    structures
    
    Parameters
    ----------
    sim_list : list of sim
        list of :class:`.simulation` instances, efield and environment configuration
        must be identical for the different simulations
    
    
    Returns
    -------
    sim : :class:`.simulation`
        new simulation with combined geometry
    
    """
    import copy
    combined_sim = copy.deepcopy(sim_list[0])
    
    combined_geo = []
    combined_pola_tensorEE = []
    combined_pola_tensorHH = []
    combined_selftermEE = []
    combined_selftermHH = []
    for sim in sim_list:
        if len(sim.efield.kwargs_permutations) != len(combined_sim.efield.kwargs_permutations):
            raise ValueError("Unequal simulation configuration. Same number of incident field configs required!")
        if not np.all(sim.efield.wavelengths == combined_sim.efield.wavelengths):
            raise ValueError("Unequal simulation configuration. Same wavelengths required!")
        if sim.efield.field_generator.__name__ != combined_sim.efield.field_generator.__name__:
            raise ValueError("Unequal simulation configuration. Same field generator!")
        if ( (sim.dyads.n1_material.__name__ != combined_sim.dyads.n1_material.__name__) or 
             (sim.dyads.n2_material.__name__ != combined_sim.dyads.n2_material.__name__) or 
             (sim.dyads.n3_material.__name__ != combined_sim.dyads.n3_material.__name__)):
            raise ValueError("Unequal environment configuration. Same refractive indices n1, n2 and n3 required!")
        if sim.struct.step != combined_sim.struct.step:
            raise ValueError("Unequal stepsize. Same step required!")
        if sim.struct.normalization != combined_sim.struct.normalization:
            raise ValueError("Unequal mesh. Same mesh required!")
        if sim.dyads.spacing != combined_sim.dyads.spacing:
            raise ValueError("Unequal spacing parameter. Same spacing size required!")
        
        combined_geo.append(copy.deepcopy(sim.struct.geometry))
        combined_pola_tensorEE.append(copy.deepcopy(sim.dyads.polarizability_EE))
        combined_pola_tensorHH.append(copy.deepcopy(sim.dyads.polarizability_HH))
        combined_selftermEE.append(copy.deepcopy(sim.dyads.self_term_tensors_EE))
        combined_selftermHH.append(copy.deepcopy(sim.dyads.self_term_tensors_HH))
    
    ## -- combined `struct` instance
    combined_sim.struct = alphaStruct(np.concatenate(combined_geo).astype(sim.dtypef))
    combined_sim.dyads = alpha_dyads_G_123_quasistatic(
                    #
                    alpha_EE=np.concatenate(combined_pola_tensorEE, axis=1).astype(sim.dtypec), 
                    alpha_HH=np.concatenate(combined_pola_tensorHH, axis=1).astype(sim.dtypec), 
                    selfterms_EE=np.concatenate(combined_selftermEE, axis=1).astype(sim.dtypec), 
                    selfterms_HH=np.concatenate(combined_selftermHH, axis=1).astype(sim.dtypec), 
                    #
                    wavelengths=combined_sim.dyads.wavelengths,
                    n1=sim.dyads.n1_material, n2=sim.dyads.n2_material, 
                    n3=sim.dyads.n3_material, spacing=sim.dyads.spacing, 
                    radiative_correction=sim.dyads.radiative_correction
                    )
    
    combined_sim.dyads._legacyStructCompatibility(combined_sim.struct)
    
    ## -- combined pre-calculated E-fields (if available)
    if combined_sim.E is not None:
        for i_E in range(len(combined_sim.E)):
            combined_E = []
            for sim in sim_list:
                combined_E.append(sim.E[i_E][1])
            combined_sim.E[i_E][1] = np.concatenate(combined_E, axis=0).astype(sim.dtypec)
    
    ## test minimum distances to next neighbor on combined geometry. 
    ## must not be < step
    from scipy.spatial import cKDTree as KDTree
    kdtree = KDTree(combined_sim.struct.geometry)
    mindist = kdtree.query(combined_sim.struct.geometry, k=2, 
                 distance_upper_bound=combined_sim.struct.step*2)[0].T[1]
    if mindist.min() < (0.99 * combined_sim.struct.step / combined_sim.struct.normalization):
        raise ValueError("Too small distance between neighbor meshpoints detected. " +
                         "Minimum allowed distance between cells is one stepsize.")
        
    return combined_sim



def convert_to_alpha_sim(sim):
    """
    convert 'conventional' pyGDM2 simulation to alpha-EE-HH simulation
    """
    from pyGDM2 import core
    geo = sim.struct.geometry
    
    pola_tensorEE = []
    selftermEE = []
    for iwl, wl in enumerate(sim.efield.wavelengths):
        pola_tensorEE.append(sim.dyads.getPolarizabilityTensor(wl, sim.struct))
        selftermEE.append(sim.dyads.getSelfTermEE(wl, sim.struct))
    
    
    alpha_struct = alphaStruct(geo)
    alpha_dyads = alpha_dyads_G_123_quasistatic(
                    #
                    alpha_EE=np.array(pola_tensorEE, dtype=sim.dtypec), 
                    # alpha_HH=pola_tensorHH, 
                    selfterms_EE=np.array(selftermEE, dtype=sim.dtypec), 
                    #
                    wavelengths=sim.efield.wavelengths,
                    n1=sim.dyads.n1_material, n2=sim.dyads.n2_material, 
                    n3=sim.dyads.n3_material, spacing=sim.dyads.spacing, 
                    radiative_correction=sim.dyads.radiative_correction
                    )
    
    alpha_sim = core.simulation(alpha_struct, sim.efield, alpha_dyads)
    
    return alpha_sim



def create_mixed_discretization_simulation(geo_definition, return_full_sim_list=False):
    """
    `geo_definitions` defines the positions of nano-blocks via their effective 
    polarizability tensor dictionaries
    
    list of elements: [[posX, posY], alpha_EE_HH_dict, discretize?]
    
    
    - position: optionally a posZ can be given. if not given, use z0 of alpha-dict
    
    - alpha_EE_HH_dict: alphadict containing both, EE and HH polarizabilities
    
    - discretize?: bool, if True, use full discretization, if False or not given, use ED/MD dipole model
    
    """
    from pyGDM2 import core
    ## generate mixed discretization simulation
    full_sim_list = []
    mixed_asim_list = []
    for _g in geo_definition:
        if len(_g)==2:
            _g.append(False)
            
        _alpha_ee_hh = _g[1]
        # a polarizability dict is given
        if type(_alpha_ee_hh) == dict:
            _s = _alpha_ee_hh['sim'].copy()
            x0 = _alpha_ee_hh['r0'][0]
            y0 = _alpha_ee_hh['r0'][1]
            z0 = _alpha_ee_hh['r0'][2]
            # _s.struct += [x0,y0,z0]
            full_discretization = _g[2]
            pass
        # a full discretized simulation is given
        elif type(_alpha_ee_hh) == core.simulation:
            _s = _alpha_ee_hh
            x0, y0, z0 = 0, 0, 0
            full_discretization = True
            pass    
        else:
            raise ValueError("simulation must be defined either as pyGDM sim-object" + 
                             " or as polarizability dictionary.")
        
        
        # shift position 
        _p = _g[0]
        if len(_p) == 2:
            _p.append(z0)
        elif len(_p) == 3 and not type(_alpha_ee_hh) == core.simulation:
            _p[2] += z0
        
        r_alpha = np.array(_p)
        _s.struct.geometry = _s.struct.geometry + np.array([_p[0]-x0, _p[1]-y0, _p[2]-z0])
        

        # add the constituent to the list
        if full_discretization==True:
            ## full discretization
            _s_disc_a = convert_to_alpha_sim(_s)
            mixed_asim_list.append(_s_disc_a)
        else:
            ## dipolar ED/MD pola model:
            ## create polarizability-based sim instance
            n1, n2, n3 = _s.dyads.n1_material, _s.dyads.n2_material, _s.dyads.n3_material
            spacing = _s.dyads.spacing
            dyads_EM = alpha_dyads_G_123_quasistatic(alpha_EE=[_alpha_ee_hh], 
                                                     n1=n1, n2=n2, n3=n3, spacing=spacing)
            struct_EM = alphaStruct(np.array([r_alpha]))
            _alphasim = core.simulation(struct_EM, _s.efield, dyads_EM)
            
            mixed_asim_list.append(_alphasim)
        
        if return_full_sim_list:
            full_sim_list.append(_s)
            
    mixed_asim = combine_alphastruct_simulations(mixed_asim_list)
    
    if return_full_sim_list:
        return mixed_asim, full_sim_list
    else:
        return mixed_asim



def extract_full_discretization_subsim(mixed_discret_sim, alpha_split, pos_alpha, verbose=0):
    """split structure in simulation and return two sim with the sub-geometries
    
    Can be used to isolate part of a complex / composed nanostructure after 
    the simulation, in order to study optical interaction effects
    
    
    Parameters
    ----------
    mixed_discret_sim : :class:`.simulation`
        mixed discretization simulation, instance of :class:`.simulation`, 
        efield may be pre-calculated, in which case they will be also split 
        and returned in the splitted simulations
    
    alpha_split : alpha-dictionary to use for extraction of subsim
        as returned by polarizabilities.get_eff_polarizability_EE_HH_viaK
    
    pos_alpha : posiotion at which alpha_split exists in mixed_discret_sim
        list of coordinates to be split off the simulation geometry or simulation 
        in which case its geometry will be extracted and used.
    
    verbose : bool, default: False
        print some info
    
    Returns
    -------
    :class:`.simulation` instance
        new simulation with splitted geometry and identical parameters as 
        defined in alpha_split as well as pre-calculated fields
    
    """
    mixed_sim = mixed_discret_sim.copy()
    sim = alpha_split['sim']
    _p = copy.deepcopy(pos_alpha)
    if len(_p)==2:
        _p.append(alpha_split['r0'][2])
    geo_sp = sim.struct.geometry + np.array([_p[0] - alpha_split['r0'][0], 
                                             _p[1] - alpha_split['r0'][1], 
                                             _p[2] - alpha_split['r0'][2]])
    
    
    step = sim.struct.step
    geo = mixed_sim.struct.geometry.copy()
    idx_split = []
    for i, pos in enumerate(geo):
        ## test if 'pos' exists in geometry
        if np.linalg.norm(geo_sp - pos, axis=1).min() < step/4 :
            idx_split.append(i)
    
    if len(idx_split) == 0:
        warnings.warn("No meshpoints in agreement with split-list detected. Splitted sim has empty geometry!")
    
    ## split part
    sim_split = copy.deepcopy(sim)
    sim_split.struct.geometry = geo[idx_split]
    sim_split.struct.n_dipoles = len(sim_split.struct.geometry)
    sim_split.struct.material = np.array(sim_split.struct.material)
    
    
    ## if available, split pre-calculated internal E-fields
    if mixed_sim.E is not None:
        sim_split.E = mixed_sim.E
        for i in range(len(mixed_discret_sim.E)):
            sim_split.E[i][1] = sim_split.E[i][1][idx_split]
    
    ## if available, split pre-calculated internal H-fields
    if mixed_sim.H is not None:
        sim_split.H = mixed_sim.H
        for i in range(len(mixed_discret_sim.H)):
            sim_split.H[i][1] = sim_split.H[i][1][idx_split]
    
    if verbose:
        print("number of original sim meshpoints: {}".format(len(geo)))
        print("number of search meshpoints:       {}".format(len(geo_sp)))
        print("number of sim1 meshpoints:         {}".format(len(sim_split.struct.geometry)))
    
    return sim_split

#%% some linear post-processing functions
from pygdmtools import polarizabilities as pola

def get_dipole_moments(sim_p_m, field_index):
    """
    returns lists of p, m
    """
    kw = sim_p_m.E[field_index][0]
    wavelength = kw['wavelength']
    alpha_EE = sim_p_m.dyads.getPolarizabilityTensorEE(wavelength, sim_p_m.struct)
    alpha_HH = sim_p_m.dyads.getPolarizabilityTensorHH(wavelength, sim_p_m.struct)
    E = sim_p_m.E[field_index][1]
    H = sim_p_m.H[field_index][1]
    P = np.matmul(alpha_EE, E[...,None])[...,0]
    M = np.matmul(alpha_HH, H[...,None])[...,0]
    
    return P, M
    
    
    
def nearfield_(sim_p_m, field_index, r_probe):#, which_fields=["Es","Et","Bs","Bt"]):
    """
    returns Es, Et, E0
    """
    kw = sim_p_m.E[field_index][0]
    wavelength = kw['wavelength']
    alpha_EE = sim_p_m.dyads.getPolarizabilityTensorEE(wavelength, sim_p_m.struct)
    alpha_HH = sim_p_m.dyads.getPolarizabilityTensorHH(wavelength, sim_p_m.struct)
    E = sim_p_m.E[field_index][1]
    H = sim_p_m.H[field_index][1]
    P = np.matmul(alpha_EE, E[...,None])[...,0]
    M = np.matmul(alpha_HH, H[...,None])[...,0]
    
    env_dict = sim_p_m.dyads.getConfigDictG(wavelength, sim_p_m.struct, sim_p_m.efield)
    E0 = sim_p_m.efield.field_generator(r_probe, env_dict, **kw)
    
    Es = pola.repropagate_ED_MD_nf(
                sim_p_m.struct.geometry, sim_p_m.struct.geometry,
                P, M, sim_p_m, wavelength, r_probe)
    Et = Es.copy()
    Et[:,3:] += E0

## !!! TODO: Magnetic field and config which fields to return
    return [Es, Et, np.concatenate([Es[:, :3], E0], axis=-1)]
    
    
def nearfield(sim_pm, field_index, r_probe, which_fields=["Es","Et","Bs","Bt"]):
    """
    `which_fields` can contain following: Es, Et, E0, Bs, Bt, B0
    """    
    from pyGDM2.multipole import _multipole_nearfield
    
    # incident field
    kw = sim_pm.E[field_index][0]
    wavelength = kw['wavelength']
    env_dict = sim_pm.dyads.getConfigDictG(wavelength, sim_pm.struct, sim_pm.efield)
    E0 = sim_pm.efield.field_generator(r_probe, env_dict, **kw)
    B0 = sim_pm.efield.field_generator(r_probe, env_dict, returnField='h', **kw)
    eps_env = sim_pm.dyads.getEnvironmentIndices(wavelength, r_probe[:1])[0]  # near-field calculation must be done entirely in one zone!
    
    # scattered field
    p, m = get_dipole_moments(sim_pm, field_index=field_index)
    r0_list = sim_pm.struct.geometry

    Es, Bs = _multipole_nearfield(
            wavelength, r_probe, eps_env=eps_env, 
            p=p, m=m, r_p=r0_list, r_m=r0_list
            )
    
    # total field and formatting
    Et = Es.copy()
    Et[:,3:] += E0
    E0 = np.concatenate([Es[:, :3], E0], axis=-1)  # add positions
    
    Bt = Bs.copy()
    Bt[:,3:] += B0
    B0 = np.concatenate([Bs[:, :3], B0], axis=-1)  # add positions
    
    # return what is requested:
    return_field_list = []
    for f_type in which_fields:
        
        if f_type.lower() == "es":
            return_field_list.append(Es)
        if f_type.lower() == "et":
            return_field_list.append(Et)
        if f_type.lower() == "e0":
            return_field_list.append(E0)
            
        if f_type.lower() in ["bs", "hs"]:
            return_field_list.append(Bs)
        if f_type.lower() in ["bt", "ht"]:
            return_field_list.append(Bt)
        if f_type.lower() in ["b0", "h0"]:
            return_field_list.append(B0)
    
    return return_field_list
    
    

def farfield(sim_p_m, field_index, r=100000, 
             tetamin=0, tetamax=np.pi, Nteta=36, 
             phimin=0, phimax=0, Nphi=1,
             return_value="map", polarizerangle='none'):
    """
    returns tetalist, philist, scattered intensity, scattered E-field in FF
    
    polarizerangle : float or 'none', default: 'none'
        optional polarization filter angle **in degrees**(!). If 'none' (default), 
        the total field-intensity is calculated (= no polarization filter)
        
    Notes
    -----
    
    Does only support vacuum environment so far!!!
    
    """
    warnings.warn("e-m-mixed discretization sim `farfield` routine is an experimental " +
                  "feature and supports ONLY VACUUM ENVIRONMENT!!")
    if str(polarizerangle).lower() == 'none':
        polarizer = 0
    else:
        polarizer = polarizerangle * np.pi/180.
    
    kw = sim_p_m.E[field_index][0]
    wavelength = kw['wavelength']
    alpha_EE = sim_p_m.dyads.getPolarizabilityTensorEE(wavelength, sim_p_m.struct)
    alpha_HH = sim_p_m.dyads.getPolarizabilityTensorHH(wavelength, sim_p_m.struct)
    E = sim_p_m.E[field_index][1]
    H = sim_p_m.H[field_index][1]
    P = np.matmul(alpha_EE, E[...,None])[...,0]
    M = np.matmul(alpha_HH, H[...,None])[...,0]
    
    # env_dict = sim_p_m.dyads.getConfigDictG(wavelength, sim_p_m.struct, sim_p_m.efield)
    # E0 = sim_p_m.efield.field_generator(r_probe, env_dict, **kw)
    
    ## so far using vacuum Green's tensors !!
    tetalist, philist, I_s, NF_dp = pola.repropagate_ED_MD_ff(
                sim_p_m.struct.geometry, sim_p_m.struct.geometry, 
                P, M, sim_p_m, wavelength,
                eps1=1, eps2=1, eps3=1, spacing=5000,
                r=r, 
                tetamin=tetamin, tetamax=tetamax, Nteta=Nteta, 
                phimin=phimin, phimax=phimax, Nphi=Nphi
                )
    
    if str(polarizerangle).lower() != 'none':
        Escat = NF_dp[:, 3:]
        
        ## --- scattered E-field parallel and perpendicular to scattering plane
        Es_par  = ( Escat.T[0] * np.cos(tetalist.flatten()) * np.cos(philist.flatten()) + 
                    Escat.T[1] * np.sin(philist.flatten()) * np.cos(tetalist.flatten()) - 
                    Escat.T[2] * np.sin(tetalist.flatten()) )
        Es_perp = ( Escat.T[0] * np.sin(philist.flatten()) - Escat.T[1] * np.cos(philist.flatten()) )
        ## --- scattered E-field parallel to polarizer
        Es_pol  = ( Es_par * np.cos(polarizer - philist.flatten()) - 
                    Es_perp * np.sin(polarizer - philist.flatten()) )
        
        NF_dp = np.concatenate([NF_dp[:, :3], Escat], axis=-1)
        I_s = np.abs(Es_pol)**2
        # 0/0
    
## !!! TODO: support asymptotic Green's tensor for a substrate
    if return_value.lower() == "map":
        return tetalist, philist, I_s.reshape(tetalist.shape), None, None
    elif return_value.lower() == "efield":
        return tetalist, philist, NF_dp, None, None
    elif return_value.lower() == "int_es":
        dteta = (tetamax-tetamin)/float(Nteta-1)
        dphi = 2.*np.pi/float(Nphi)
        d_solid_surf = r**2 * np.sin(tetalist) * dteta * dphi
        Iint = np.sum(I_s * d_solid_surf.flatten())
        return Iint
    else:
        raise ValueError("`return_value` currently supports only 'map', 'efield' or 'int_es'.")


def extinct(sim_pm, field_index):
    """total extinction cross-sections of all electric dipoles and all magnetic dipoles
    """
    from pyGDM2.multipole import _multipole_extinct
    
    p, m = get_dipole_moments(sim_pm, field_index=field_index)
    r0_list = sim_pm.struct.geometry

    ecs_p, ecs_m, ecs_Qe, ecs_Qm = _multipole_extinct(
            sim_pm, field_index=field_index,
            p=p, m=m, r_p=r0_list, r_m=r0_list,
            normalization_E0=False
            )
    
    return ecs_p, ecs_m


def scs(sim_pm, field_index):
    """total extinction cross-sections of all electric dipoles and all magnetic dipoles
    """
    from pyGDM2.multipole import _multipole_scs
    
    p, m = get_dipole_moments(sim_pm, field_index=field_index)
    r0_list = sim_pm.struct.geometry

    scs_p, scs_m, scs_Qe, scs_Qm = _multipole_scs(
            sim_pm, field_index=field_index,
            p=p, m=m, r_p=r0_list, r_m=r0_list,
            normalization_E0=False
            )

    return scs_p, scs_m

def visu_mixed_structure(sim_mixed, sim_list=None, scale=1, 
                         projection='xy', ax=None, show=True):
    import matplotlib.pyplot as plt
    from pyGDM2 import visu

    if ax is None and show==True:
        fig = plt.figure()
        ax = plt.subplot(aspect='equal')
    
    visu.structure(sim_mixed, 
                scale=scale,
                projection=projection, show=0)

    if sim_list is not None:
        for _subsim in sim_list:
            visu.structure_contour(_subsim, projection=projection, show=0)

    if show:
        plt.show()




#%% testing
# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
# testing
# =============================================================================
# =============================================================================
# =============================================================================
# =============================================================================
if __name__ == '__main__':
    method = 'lu'
    
    import pickle
    import matplotlib.pyplot as plt

    from pygdmtools.polarizabilities import repropagate_ED_MD_nf
    from pyGDM2 import fields
    from pyGDM2 import core
    from pyGDM2 import materials
    from pyGDM2 import linear
    from pyGDM2 import visu
    from pyGDM2 import tools
    
    #%%
    ## some general config
    status_plotting = 0
    
    # method = 'lu'
    method = 'cupy'
    R_fulldiscret = 500
    

    ## define complex composed structure
    alpha_ee_hh = pickle.load(open('test_pola_dielectric_EE_HH.pkl', 'rb'))
    
    
    ## if 2D positions: use z0 of alpha-dict as z-coordinate for dipole
    ## bool: whether or not use full disctretization
    geo_definition = [
        [[   0,    0], alpha_ee_hh, False],
        # [[-160, -190], alpha_ee_hh, False],
        # [[  90, -160], alpha_ee_hh, False],
        # [[ -70,  170], alpha_ee_hh, False],
        # [[ 300, -100], alpha_ee_hh, False],
        # [[-200, -350], alpha_ee_hh, False],
        # [[ 100,  350], alpha_ee_hh, False],
        # [[-280,  150], alpha_ee_hh, False],
        # [[ 290,  100], alpha_ee_hh, False],
        # [[-370, -130], alpha_ee_hh, False],
        # [[-500, -330], alpha_ee_hh, False],
        # [[ 450,  130], alpha_ee_hh, False],
        # [[-620, -130], alpha_ee_hh, False],
        [[ 650,  30], alpha_ee_hh, False],
            ]
    
    full_alpha_sim, full_sim_list = create_mixed_discretization_simulation(geo_definition, return_full_sim_list=True)
    fullsim = sum_of_list(full_sim_list, len(full_sim_list)-1)

    visu.structure(fullsim, tit='N dipoles={}'.format(len(fullsim.struct.geometry)))
    
    
    #%%
    ## define alternative efield
    wavelengths = [550.0]
    field_generator = fields.plane_wave
    field_kwargs = dict(E_s=1, E_p=1, inc_angle=0, inc_plane='xz') # lin-pol X, normal incidence
    field_kwargs = dict(E_s=1, E_p=1, inc_angle=45, inc_plane='xz') # lin-pol X, normal incidence
    # field_kwargs = dict(E_s=0, E_p=1, inc_angle=0, inc_plane='xz') # lin-pol X, normal incidence
    
    efield = fields.efield(field_generator, wavelengths=wavelengths, kwargs=copy.deepcopy(field_kwargs))
    
    
    
    
    
    
    ## generate mixed discretization simulation for each sub-element with radius of full discretizaation
    t0=time.time()
    reconstr_sim_list = []
    for _g1 in geo_definition[:]:
        _p_center = _g1[0]
        _alpha_center = _g1[1]
        _geo_def_local = copy.deepcopy(geo_definition)
        for _g2 in _geo_def_local:
            _Dp = np.array(_g2[0]) - np.array(_p_center)
            ## position inside radius: full discretization, else ED/MD pola
            if _Dp[0]**2 + _Dp[1]**2 < R_fulldiscret**2:
                _g2[2] = True
            else:
                _g2[2] = False
    
        _mixed_asim = create_mixed_discretization_simulation(_geo_def_local)
        _mixed_asim.efield = efield
        
        
        ## plot structure 
        if status_plotting:
            plt.subplot(aspect='equal')
            visu.structure(fullsim, show=0, scale=2, color='.7')
            visu.structure(_mixed_asim, scale=25, borders=100, show=0, color='C0')
            plt.scatter([_p_center[0]], [_p_center[1]], color='C1', marker='x', s=150, lw=3)
            circle1 = plt.Circle((_p_center[0], _p_center[1]), R_fulldiscret, color='C1', fill=False)
            plt.gca().add_patch(circle1)
            plt.show()
        
        scatter(_mixed_asim, method=method)
        
        subsim_reconstr_from_mixed = extract_full_discretization_subsim(
                        _mixed_asim, _alpha_center, _p_center, verbose=0)
        # visu.structure(subsim_reconstr_from_mixed)
        reconstr_sim_list.append(subsim_reconstr_from_mixed.copy())
    
    full_reconstr_sim = sum_of_list(reconstr_sim_list, len(reconstr_sim_list)-1)
    t1=time.time()
    print('loop time: ', t1-t0)
    
    
    
    
    ## replace efield
    fullsim.efield = efield
    full_alpha_sim.efield = efield
    full_reconstr_sim.efield = efield
    
    
    #%%
    core.scatter(fullsim, method=method)
    scatter(full_alpha_sim, method=method)
    # scatter(mixed_asim, method=method)
    
    
    
    #%%
    fidx = 0
    
    kw = fullsim.E[fidx][0]
    wavelength = kw['wavelength']
    
    
    alpha_EE_fa = full_alpha_sim.dyads.getPolarizabilityTensorEE(wavelength, full_alpha_sim.struct)
    alpha_HH_fa = full_alpha_sim.dyads.getPolarizabilityTensorHH(wavelength, full_alpha_sim.struct)
    E = full_alpha_sim.E[fidx][1]
    H = full_alpha_sim.H[fidx][1]
    P_fa = np.matmul(alpha_EE_fa, E[...,None])[...,0]
    M_fa = np.matmul(alpha_HH_fa, H[...,None])[...,0]
    
    
    
    
    
    r_probe = tools.generate_NF_map(-1500, 1500, 51, -1500, 1500, 51, 300).astype(np.float32); proj='xy'
    # r_probe = tools.generate_NF_map_XZ(-1000, 500, 51, -1000, 500, 51, 200).astype(np.float32); proj='xz'
    
    
    
    
    Es, Et, E0 = linear.nearfield(fullsim, fidx, r_probe, which_fields=['es', 'et', 'e0'])
    Es_fullreconstr, Et_fullreconstr, E0_fullreconstr = linear.nearfield(full_reconstr_sim, fidx, r_probe, which_fields=['es', 'et', 'e0'])
    
    # Es_alphaonly_simEH = repropagate_ED_MD_nf(full_alpha_sim.struct.geometry, full_alpha_sim.struct.geometry,
    #                                           P_fa, M_fa, full_alpha_sim, wavelength, r_probe)
    # Et_alphaonly_simEH = Es_alphaonly_simEH.copy()
    # Et_alphaonly_simEH[:,3:] += E0[:,3:]
    
    Es_alphaonly_simEH, Et_alphaonly_simEH, E0_alphaonly_simEH = nearfield(full_alpha_sim, fidx, r_probe)
    
    
    p_list_sim = []
    m_list_sim = []
    pos_list = []
    for i, _sim in enumerate(full_sim_list):
        sim_split, remain_sim = tools.split_simulation(fullsim, _sim)
        p, m, _rp,_rm = pola.decomp_ed_md(sim_split, field_index=fidx)
        pos_list.append(_rp)
        p_list_sim.append(p)
        m_list_sim.append(m)
    
    p_list_sim = np.array(p_list_sim, dtype=np.complex64)
    m_list_sim = np.array(m_list_sim, dtype=np.complex64)
    pos_list = np.array(pos_list, dtype=np.float32)
    
    Es_simDP = repropagate_ED_MD_nf(pos_list, pos_list, p_list_sim, m_list_sim, fullsim, wavelength, r_probe)
    Et_simDP = Es_simDP.copy()
    Et_simDP[:,3:] += E0[:,3:]
    
    
    
    cmap='jet'
    plt.figure(figsize=(10,15))
    plt.subplot(321); plt.title('full sim')
    im = visu.vectorfield_color(Et, show=0, cmap=cmap)#, norm=colors.LogNorm())
    plt.colorbar(im)
    clim = im.get_clim()
    visu.structure_contour(fullsim, show=0, projection=proj)
    
    
    plt.subplot(322); plt.title('sim - ED+MD')
    im = visu.vectorfield_color(Et_simDP, show=0, cmap=cmap)#, norm=colors.LogNorm())
    plt.colorbar(im)
    im.set_clim(clim)
    visu.structure_contour(fullsim, show=0, projection=proj)
    
    
    plt.subplot(323); plt.title('mixed discretization')
    im = visu.vectorfield_color(Et_fullreconstr, show=0, cmap=cmap)#, norm=colors.LogNorm())
    plt.colorbar(im)
    im.set_clim(clim)
    visu.structure_contour(full_reconstr_sim, show=0, projection=proj)
    
    
    plt.subplot(324); plt.title('error (%)\nmixed-discretization / full sim')
    # I_DP = Et_simDP[:,:4].copy(); I_DP[:,-1] = np.sum(np.abs(Et_simDP[:,3:])**2, axis=1) ## sim ED/MD 
    I_DP = Et[:,:4].copy(); I_DP[:,-1] = np.sum(np.abs(Et[:,3:])**2, axis=1)  ## full sim
    I_HHcpl = Et_fullreconstr[:,:4].copy()
    I_HHcpl[:,-1] = np.sum(np.abs(Et_fullreconstr[:,3:])**2, axis=1)
    err_rel = I_DP.copy()
    err_rel[:,-1] = 100*(I_HHcpl[:,-1] - I_DP[:,-1])  / I_DP[:,-1]
    im = visu.scalarfield(err_rel, show=0, cmap='bwr')#, norm=colors.LogNorm())
    # im = visu.scalarfield(I_DP, show=0, cmap='bwr')#, norm=colors.LogNorm())
    plt.colorbar(im)
    clim_err = im.get_clim()
    im.set_clim([-np.abs(clim_err).max(), np.abs(clim_err).max()])
    
    visu.structure_contour(full_reconstr_sim, show=0, projection=proj)
    
    
    
    plt.subplot(325); plt.title('only EE HH polarizabilities')
    im = visu.vectorfield_color(Et_alphaonly_simEH, show=0, cmap=cmap)#, norm=colors.LogNorm())
    plt.colorbar(im)
    im.set_clim(clim)
    visu.structure_contour(fullsim, show=0, projection=proj)
    
    plt.subplot(326); plt.title('error (%)\nEE-HH-coupled dipoles / full sim')
    I_EEHH = Et_alphaonly_simEH[:,:4].copy()
    I_EEHH[:,-1] = np.sum(np.abs(Et_alphaonly_simEH[:,3:])**2, axis=1)
    err_rel_EEHH = I_DP.copy()
    err_rel_EEHH[:,-1] = 100*(I_EEHH[:,-1] - I_DP[:,-1])  / I_DP[:,-1]
    im = visu.scalarfield(err_rel_EEHH, show=0, cmap='bwr')#, norm=colors.LogNorm())
    # im = visu.scalarfield(I_DP, show=0, cmap='bwr')#, norm=colors.LogNorm())
    plt.colorbar(im)
    clim_err = im.get_clim()
    im.set_clim([-np.abs(clim_err).max(), np.abs(clim_err).max()])
    
    visu.structure_contour(fullsim, show=0, projection=proj)
    
    
    plt.show()
    
    
    #%%
    tetalist, philist, I_s, _, _ = farfield(full_alpha_sim, fidx, r=100000, 
                                             tetamin=0, tetamax=np.pi, Nteta=36, 
                                             phimin=0, phimax=0, Nphi=1)
    Itot = farfield(full_alpha_sim, fidx, r=100000, 
                    tetamin=0, tetamax=np.pi, Nteta=36, 
                    phimin=0, phimax=0, Nphi=1, return_value='int_es')
    
    tetalist2, philist2, I_s2, I_t2, I_02 = linear.farfield(fullsim, fidx, r=100000, 
                                             tetamin=0, tetamax=np.pi, Nteta=36, 
                                             phimin=0, phimax=0, Nphi=1)
    
    plt.subplot(polar=True)
    plt.plot(tetalist, I_s)
    plt.plot(tetalist, I_s2)
    
    
    